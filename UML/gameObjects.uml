@startuml

hide empty members
skinparam classAttributeIconSize 0

interface Script <<Interface>> {
	+{abstract} execute(owner : GameObject, other : GameObjectList, inputs : Input, currentTime : int) : void
}

abstract class Path {
	+{abstract} move(p : Position) : Position
}

abstract class CommandMovement {
	+{abstract} move(p : Position, input : Input) : Position
}

abstract class GameObject {
	-halfSize : float
	-position : Position
	+GameObject(halfSize : float, position : Position)
	+getPosition() : Position
	+setPosition(position : Position)
	+getHalfSize() : float
	+addScripts(scripts : Script...) : void
	+removeScript(script : Script) : void
	+executeScripts(other : GameObjectList, input : Input, currentTime : int) : void
	+{abstract} accept(script : GameObjectVisitor) : void
}

class Ally #back:lightgreen {
	-angleShot : float
	-angleShotFocus : float
	+Ally(p : Player, angleShot : float, angleShotFocus : float, path : CommandMovement)
	+getPlayer() : Player
	+getAngleShot() : float
	+getAngleShotFocus() : float
	+accept(script : GameObjectVisitor) : void
}

abstract class Bullet {
	-damage : int
	#Bullet(position : Position, halfSize : float, damage : int, path : Path)
	+getDamage() : int
}

abstract class CommandObject {
	#CommandObject(halfSize : float, position : Position, movement : CommandMovement)
	+getMovement() : CommandMovement
}

class Decoration #back:lightgreen {
	+Decoration(position : Position, halfSize : float, path : Path)
	+accept(script : GameObjectVisitor) : void
}

class Enemy #back:lightgreen {
	-isAlive : boolean
	-hp : int
	+Enemy(position : Position, halfSize : float, path : Path, hp : int)
	-void randomRewards(gameObjectList : GameObjectList)
	+receiveDamage(damage : int, gameObjectList : GameObjectList) : void
	+accept(script : GameObjectVisitor) : void
}

class EnemyBullet #back:lightgreen {
	+EnemyBullet(position : Position, halfSize : float, path : Path, damage : int)
	+accept(script : GameObjectVisitor) : void
}

abstract class Item #back:lightgreen {
	#Item(position : Position, halfSize : float)
	+attractByPlayer(p : Player) : void
	+{abstract} reward(p : Player) : void
	+accept(script : GameObjectVisitor) : void
}

abstract class PathObject {
	#PathObject(position : Position, halfSize : float, path : Path)
	+getPath() : Path
	#setPath(path : Path) : void
}

class Player #back:lightgreen {
	-speed : float
	-power : int
	-hp : int
	+Player(position : Position, halfSize : float, hp : int, speed : float)
	+getSpeed() : float
	+getPower() : int
	+getHp() : int
	+setPower(power : int)
	+receiveMaxDamage() : void
	+receiveDamage(damage : int) : void
	+accept(script : GameObjectVisitor) : void
}

class PlayerBullet #back:lightgreen {
	+PlayerBullet(position : Position, path : Path, damage : int)
	+accept(script : GameObjectVisitor) : void
}

class PowerItem {
	+PowerItem(position : Position, halfSize : float)
	+reward(p : Player) : void
}

class GameObjectList {
	+GameObjectList(player : Player)
	+add(go : GameObject)
	+remove(go : GameObject)
	+update()
	+bulletCount() : int
	+enemies() : Iterable<Enemy>
	+items() : Iterable<Item>
	+player() : Player
	+iterator() : Iterator<GameObject>
}

note "Les classes coloriees en vert sont les classes qui peuvent etre visitees par le pattern visiteur" as notecouleur

GameObjectList "1" *-r- "*" GameObject
note on link : Dans le modele interne de la classe GameObjectList,\nil  y en realite plusieurs listes mais l'UML ne represente\nqu'un seul trait pour des questions de lisibilite.\nIl y a aussi une liste des objects qui seront ajoute\net une liste des objets qui seront supprimes a la prochaine frame.

GameObject <|-- CommandObject
CommandObject <|-- Player
CommandObject <|-- Ally
GameObject <|-- PathObject
PathObject <|-- Decoration
PathObject <|-- Enemy
PathObject <|-- Bullet
Bullet <|-- EnemyBullet
Bullet <|-- PlayerBullet
PathObject <|-- Item
Item <|-- PowerItem

Ally "1" o-r- "*" Player
CommandObject "1" *-r- "1" CommandMovement
PathObject "1" *-r- "1" Path

GameObject "1" *-r- "*" Script

@enduml