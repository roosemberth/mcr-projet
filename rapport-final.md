# Rapport final Projet MCR

Date : 21 Juin 2021

Professeur et assitants:

- Rosat Sébastien
- Decorvet Grégoire
- Sousa e Silva Fábio

Auteurs :

- Roosembert Palacios
- Gazzetta Florian
- Wilfried Karel Ngueukam Djeuda
- Junod Christophe
- Mai Hoang Anh

---

## Mise en Oeuvre du modèle au sein de l'application

---
Les jeux du type *__Shoot’em up__* comportent généralement une multitude d’objets qui interagissent entre eux. Gérer ses interactions et les objets peut s’avérer très complexes. Un multitude de balles, présence de plusieurs vaisseaux, et item diverses. Afin de gérer les objets du jeu, Nous avons utiliser le pattern visiteur nous permettant ainsi de simplifier la complexité que représentent la gestion des multiples interactions entre tous les objets du jeu.
Cette simplification se traduit par le fait que ce n’est pas les objets du jeu (que nous appellerons gameObject par la suite) qui définissent comment ils interagissent entre eux mais nos visiteurs. De ce fait, la logique de l'implémentation est réalisée dans un seul endroit (dans les visiteurs). Le changement de cette logique est ainsi plus aisé et la maintenance du code se trouve grandement simplifiée.

Nous n’avons cependant pas ajouté le pattern visiteur à tous les endroits du code mais principalement dans les endroits où cela représentait un réel avantage.
Ainsi donc, dans notre application, les classes susceptibles de se faire visiter sont les suivants :

- *__Player__* : Représente le vaisseau du joueur
- *__Enemy__* : Représente les vaisseaux ennemis à détruire
- *__EnemyBullet__* : Les balles tirés par des ennemis
- *__PlayerBullet__* : Balles tirés par les joueurs
- *__Item__* : différents objets qui apparaissent permettant au joueur de bénéficier d’avantages diverses
- *__Decoration__* : Décoration particulière suite à une action particulière
- *__Ally__* : armes supplémentaires ajouter au joueur

Ceux susceptibles de les visiter sont les suivants :

- *__RenderEngine__* : S’occupe d’afficher les gameObject.
- *__CollissionEngine__* : Gère la collision entre 2 gameObjets
- *__MovementEngine__* : Gère le mouvement des objets et s’occupe de les supprimer quand ils sortent de l’écran.
- *__GameObjectList__* : Utilisent 2 classes anonymes qui implémentent des méthodes visiteurs permettant d’ajouter ou supprimer des gameObject.

Ainsi donc, le *__RenderEngine__* visite tous les objets et les affiche dans la fenêtre. Le *__CollisionEngine__* s'occupe de la collision entre 2 objets qui peuvent entrer en colission. Elle vérifie si 2 objets entrent en colission et applique le comportement attendu. *__MovementEngine__* gèrent les mouvements des objets et s'assurent que tous les objets sont supprimés une fois qu'ils sortent de la fenêtre. *__GameObjectList__* utilisent 2 classes anonymes implémentant l'interface *__GameObjectVisitor__* définissant des méthodes permettant de visiter des _gameObject_. Ces deux classes anonymes sont utilisées pour retirer ou ajouter des gameObjets dans la *__GameObjectList__*.

## Diagramme de classe

```plantuml
@startuml

hide empty members
skinparam classAttributeIconSize 0

abstract class CommandMovement {
    +{abstract} move(p : Position, input : Input) : Position
}

class PlayerMovement {
    -owner : Player
    +setOwner(player : Player)
    +move(p : Position, input : Input) : Position
}

abstract class RelativeMovement {
    -source : GameObject
    -relativePosition : Vector
    #RelativeMovement(source : GameObject, initialRelativePosition : Vector)
    +move(p : Position, input : Input) : Position
    #{abstract} moveRelative(relativePosition : Vector, input : Input) : Vector
}

class AllyShotTypeAPath {
    -relativeTarget : Vector
    -relativeTargetFocus : Vector
    -speed : float
    +AllyShotTypeAPath(owner : Player, relativeTarget : Vector, relativeTargetFocus : Vector, speed : float)
    #moveRelative(relativePosition : Vector, input : Input) : Vector
}

class AllyShotTypeBPath {
    -disMax: float
    -disMin: float
    -angle: float
    -disCurrent: float
    +AllyShotTypeBPath(owner : Player, angle : float, distance : float)
    #moveRelative(relativePosition : Vector, input : Input) : Vector
}

class AllyShotTypeCPath {
    -target : GameObject
    -distance : float
    +AllyShotTypeCPath(target : GameObject, distance : float)
    +move(p : Position, input : Input) : Position
}

CommandMovement <|-- PlayerMovement
CommandMovement <|-- RelativeMovement
RelativeMovement <|-- AllyShotTypeAPath
RelativeMovement <|-- AllyShotTypeBPath
CommandMovement <|-- AllyShotTypeCPath

@enduml
```

Représente le déplacement du joueur et de ces alliés en fonction des touches du clavier.
<br/><br/><br/>

```plantuml
@startuml

hide empty members
skinparam classAttributeIconSize 0

interface Script <<Interface>> {
    +{abstract} execute(owner : GameObject, other : GameObjectList, inputs : Input, currentTime : int) : void
}

abstract class Path {
    +{abstract} move(p : Position) : Position
}

abstract class CommandMovement {
    +{abstract} move(p : Position, input : Input) : Position
}

abstract class GameObject {
    -halfSize : float
    -position : Position
    +GameObject(halfSize : float, position : Position)
    +getPosition() : Position
    +setPosition(position : Position)
    +getHalfSize() : float
    +addScripts(scripts : Script...) : void
    +removeScript(script : Script) : void
    +executeScripts(other : GameObjectList, input : Input, currentTime : int) : void
    +{abstract} accept(script : GameObjectVisitor) : void
}

class Ally #back:lightgreen {
    -angleShot : float
    -angleShotFocus : float
    +Ally(p : Player, angleShot : float, angleShotFocus : float, path : CommandMovement)
    +getPlayer() : Player
    +getAngleShot() : float
    +getAngleShotFocus() : float
    +accept(script : GameObjectVisitor) : void
}

abstract class Bullet {
    -damage : int
    #Bullet(position : Position, halfSize : float, damage : int, path : Path)
    +getDamage() : int
}

abstract class CommandObject {
    #CommandObject(halfSize : float, position : Position, movement : CommandMovement)
    +getMovement() : CommandMovement
}

class Decoration #back:lightgreen {
    +Decoration(position : Position, halfSize : float, path : Path)
    +accept(script : GameObjectVisitor) : void
}

class Enemy #back:lightgreen {
    -isAlive : boolean
    -hp : int
    +Enemy(position : Position, halfSize : float, path : Path, hp : int)
    -void randomRewards(gameObjectList : GameObjectList)
    +receiveDamage(damage : int, gameObjectList : GameObjectList) : void
    +accept(script : GameObjectVisitor) : void
}

class EnemyBullet #back:lightgreen {
    +EnemyBullet(position : Position, halfSize : float, path : Path, damage : int)
    +accept(script : GameObjectVisitor) : void
}

abstract class Item #back:lightgreen {
    #Item(position : Position, halfSize : float)
    +attractByPlayer(p : Player) : void
    +{abstract} reward(p : Player) : void
    +accept(script : GameObjectVisitor) : void
}

abstract class PathObject {
    #PathObject(position : Position, halfSize : float, path : Path)
    +getPath() : Path
    #setPath(path : Path) : void
}

class Player #back:lightgreen {
    -speed : float
    -power : int
    -hp : int
    +Player(position : Position, halfSize : float, hp : int, speed : float)
    +getSpeed() : float
    +getPower() : int
    +getHp() : int
    +setPower(power : int)
    +receiveMaxDamage() : void
    +receiveDamage(damage : int) : void
    +accept(script : GameObjectVisitor) : void
}

class PlayerBullet #back:lightgreen {
    +PlayerBullet(position : Position, path : Path, damage : int)
    +accept(script : GameObjectVisitor) : void
}

class PowerItem {
    +PowerItem(position : Position, halfSize : float)
    +reward(p : Player) : void
}

class GameObjectList {
    +GameObjectList(player : Player)
    +add(go : GameObject)
    +remove(go : GameObject)
    +update()
    +bulletCount() : int
    +enemies() : Iterable<Enemy>
    +items() : Iterable<Item>
    +player() : Player
    +iterator() : Iterator<GameObject>
}

note "Les classes coloriees en vert sont les classes qui peuvent etre visitees par le pattern visiteur" as notecouleur

GameObjectList "1" *-r- "*" GameObject
note on link : Dans le modele interne de la classe GameObjectList,\nil  y en realite plusieurs listes mais l'UML ne represente\nqu'un seul trait pour des questions de lisibilite.\nIl y a aussi une liste des objects qui seront ajoute\net une liste des objets qui seront supprimes a la prochaine frame.

GameObject <|-- CommandObject
CommandObject <|-- Player
CommandObject <|-- Ally
GameObject <|-- PathObject
PathObject <|-- Decoration
PathObject <|-- Enemy
PathObject <|-- Bullet
Bullet <|-- EnemyBullet
Bullet <|-- PlayerBullet
PathObject <|-- Item
Item <|-- PowerItem

Ally "1" o-r- "*" Player
CommandObject "1" *-r- "1" CommandMovement
PathObject "1" *-r- "1" Path

GameObject "1" *-r- "*" Script

@enduml
```

Représente les relations entre les différents gameObject de notre application.
<br/><br/><br/>

```plantuml
@startuml

hide empty members
skinparam classAttributeIconSize 0

interface Script <<Interface>> {
    +{abstract} execute(owner : GameObject, other : GameObjectList, inputs : Input, currentTime : int) : void
}

abstract class ShotTypeReference {
    -{static} powerInterval : int
    -{static} maxPowerLevel : int
    -{static} shotInterval : int
    -bulletSpeed : int
    -player : Player
    -allies : List<Ally>
    -powerLevel : int
    -lastShoot : int
    +ShotTypeReference(player : Player)
    -checkLevel(other : GameObjectList) : void
    #shoot(other : GameObjectList, inputs : Input)  : void
    #{abstract} createAllies(other : GameObjectList, powerLevel : int) : void
    +execute(owner : GameObject, other : GameObjectList, inputs : Input, currentTime : int) : void
}

class ShotTypeA {
    +ShotTypeA(player : Player)
    -add2SymetricAllies(x : int, y : int, offsetAngle : float, xFocus : int, offsetAngleFocus : float) : void
    #shoot(other : GameObjectList, inputs : Input)  : void
    #createAllies(other : GameObjectList, powerLevel : int) : void
}

class ShotTypeB {
    +ShotTypeB(player : Player)
    -addAlly(anglePosition : float, distance : float) : void
    #shoot(other : GameObjectList, inputs : Input)  : void
    #createAllies(other : GameObjectList, powerLevel : int) : void
}

class ShotTypeC {
    +ShotTypeC(player : Player)
    -addAlly(tail : GameObject, distance : float)
    #shoot(other : GameObjectList, inputs : Input)  : void
    #createAllies(other : GameObjectList, powerLevel : int) : void
}

abstract class CyclingScript {
    -cycleInterval : int
    -lastCycle : int
    +execute(owner : GameObject, other : GameObjectList, inputs : Input, currentTime : int) : void
    #{abstract} executeAction(owner GameObject, other GameObjectList) : void
}

class DestroyedAtTimeOut {
    -duration : int
    +DestroyedAtTimeOut(duration : int)
    +execute(owner : GameObject, other : GameObjectList, inputs : Input, currentTime : int) : void
}

class SpinningCannon {
    -int angleOffset : int
    -int sourceCount : int
    -float spiralAccel : float
    -float bulletSpeed : float
    -int bulletDamage : int
    -angle : float
    -offset : float
    +SpinningCannon(reloadTime : int, sourceCount : int, spiralAccel : float, bulletSpeed : float, bulletDamage : int)
    #executeAction(owner GameObject, other GameObjectList) : void
}

class CurvedCircle {
    -int density : int
    -float bulletSpeed : float
    -float offset : float
    -float angle : float
    -float spinSpeed : float
    -initAngle : float
    +CurvedCircle(cycleInterval : int, density : int, bulletSpeed : float, offset : float, spinSpeed : float)
    #executeAction(owner GameObject, other GameObjectList) : void
}

class BombLauncher {
    -bulletDamage : int
    -path : LinearPath
    +BombLauncher(reloadTime : int, bulletSpeed : float, bulletDamage : int, angle : float)
    #executeAction(owner GameObject, other GameObjectList) : void
}

class AimedBombLancher {
    -offsetAngle : float
    -bulletSpeed : float
    +AimedBombLancher(reloadTime : int, bulletSpeed : float, offsetAngle : float)
    #executeAction(owner GameObject, other GameObjectList) : void
}

Script <|.. CyclingScript
Script <|.. ShotTypeReference
Script <|.. DestroyedAtTimeOut
ShotTypeReference <|-- ShotTypeA
ShotTypeReference <|-- ShotTypeB
ShotTypeReference <|-- ShotTypeC
CyclingScript <|-- CurvedCircle
CyclingScript <|-- SpinningCannon
CyclingScript <|-- BombLauncher
CyclingScript <|-- AimedBombLancher

@enduml
```

Les scripts sont les actions spécifiques que peuvent exécuter des gameObjects donnés.
<br/><br/><br/>

```plantuml
@startuml

hide empty members
skinparam classAttributeIconSize 0

abstract class Path {
    +{abstract} move(p : Position) : Position
}

class LinearPath {
    -accel : float
    -direction : Vector
    +LinearPath(v : Vector)
    +LinearPath(x : float, y : float)
    +LinearPath(v : Vector, acceleration : float)
    +LinearPath(x : float, y : float, acceleration : float)
    +move(p : Position) : Position
}

class CurvedPath {
    -speed : float
    -offset : float
    -angle : float
    +CurvedPath(initAngle : float, speed : float, offset : float)
    +move(p : Position) : Position
}

class AimedPath {
    -target : GameObject
    -speed : float
    +AimedPath(target : GameObject, speed : float)
    +move(p : Position) : Position
}

class RandomPathInBound {
    -speed : float
    -x : float
    -y : float
    -width : float
    -height : float
    -currentPath : Vector
    -dest : Position
    +RandomPathInBound(startPos : Position, maxSpeed : float, x : float, y : float, width : float, height : float)
    +move(p : Position) : Position
}

class RoundTripPath {
    -first : Position
    -second : Position
    -speed : float
    -goToSecond : boolean
    +RoundTripPath(first : Position, second : Position, speed : float)
    +move(p : Position) : Position
}

class StationaryPath {
    +move(p : Position) : Position
}

Path <|-- StationaryPath
Path <|-- RoundTripPath
Path <|-- RandomPathInBound
Path <|-- AimedPath
Path <|-- CurvedPath
Path <|-- LinearPath

@enduml
```

Permet d'implémenter la trajectoire des gameObject au sein de l'application.
<br/><br/><br/>

```plantuml
@startuml

hide empty members
skinparam classAttributeIconSize 0

interface FixedManager <<Interface>> {
    +{abstract} setCurrentFrameInfos(input : Input) : void
}

interface GameObjectVisitor <<Interface>> {
    +{abstract} visit(o : Player) : void
    +{abstract} visit(o : Enemy) : void
    +{abstract} visit(o : EnemyBullet) : void
    +{abstract} visit(o : PlayerBullet) : void
    +{abstract} visit(item : Item) : void
    +{abstract} visit(decoration : Decoration) : void
    +{abstract} visit(ally : Ally) : void
}

interface Manager <<Interface>> {
    +{abstract} getName() : String
}

class RenderEngine {
    +{static} imageType : int
    -panel : JPanelGame
    -ratio : float
    -playerSprite : AbstractSprite
    -enemySprite : AbstractSprite
    -playerSprite : AbstractSprite
    -playerBulletSprite : AbstractSprite
    -enemyBulletSprite : AbstractSprite
    -itemSprite : AbstractSprite
    -explosionSprite : AbstractSprite
    -allySprite : AbstractSprite
    +RenderEngine(p : JPanelGame)
    -display(sprite : AbstractSprite, pos : Position, g : Graphics2D)
    +setRatio(ratio : float) : void
    +getName() : String
    +visit(o : Player) : void
    +visit(o : Enemy) : void
    +visit(o : EnemyBullet) : void
    +visit(o : PlayerBullet) : void
    +visit(item : Item) : void
    +visit(decoration : Decoration) : void
    +visit(ally : Ally) : void
}

class MovementEngine {
    -gameObjectList : GameObjectList
    -currentInput : Input
    +MovementEngine(gameObjectList : GameObjectList)
    -{static} stayInBound(p : Position) : Position
    -{static} checkFullyOffScreen(o : GameObject) : boolean
    -deleteOffScreen(o : GameObject) : void
    -moveAndDeleteOffScreen(po : PathObject) : void
    +getName() : String
    +setCurrentFrameInfos(input : Input) : void
    +visit(o : Player) : void
    +visit(o : Enemy) : void
    +visit(o : EnemyBullet) : void
    +visit(o : PlayerBullet) : void
    +visit(item : Item) : void
    +visit(decoration : Decoration) : void
    +visit(ally : Ally) : void
}

class CollisionEngine {
    -gameObjectList : GameObjectList
    +CollisionEngine(gameObjectList : GameObjectList)
    +{static} checkCollision(o1 : GameObject, o2 : GameObject)
    +{static} checkCollision(p1 : Position, halfSize1 : float, p2 : Position, halfSize2 : float)
    +getName() : String
    +setCurrentFrameInfos(input : Input) : void
    +visit(o : Player) : void
    +visit(o : Enemy) : void
    +visit(o : EnemyBullet) : void
    +visit(o : PlayerBullet) : void
    +visit(item : Item) : void
    +visit(decoration : Decoration) : void
    +visit(ally : Ally) : void
}

class Level {
    +{static} PlayerSize : float
    +{static} EnemySize : float
    +{static} PlayerBulletSize : float
    +{static} EnemyBulletSize : float
    +{static} ItemSize : float
    +{static} ExplosionSize : float
    +{static} AllySize : float
    +{static} SIMULATION_WIDTH : float
    +{static} SIMULATION_HEIGHT : float
    -tickLength : long
    -timeShift : long
    -currentTickNumber : int
    +{static} createExplosion(gameObjectList : GameObjectList, nb : int, p : Position, size : float) : void
    +{static} selectLevel(options : Options) : LevelDescription
    +Level(options : Options)
    +addFixedManager(manager: Manager) : void
    +addDynamicManager(manager : FixedManager) : void
    -tick(inputs : Input, managersTime : Map<String, Long>) : void
    -tick(inputs : Input, tickNumber : int, debugInformationReceiver : DebugInformationReceiver) : void
    +getBulletCount() : int
    +update(inputs : Input, timeSinceLastFrame : long, debugInformationReceiver : DebugInformationReceiver) : GameState
}

class GameObjectList {
    +GameObjectList(player : Player)
    +add(go : GameObject)
    +remove(go : GameObject)
    +update()
    +bulletCount() : int
    +enemies() : Iterable<Enemy>
    +items() : Iterable<Item>
    +player() : Player
    +iterator() : Iterator<GameObject>
}

abstract class GameObject {
    -halfSize : float
    -position : Position
    +GameObject(halfSize : float, position : Position)
    +getPosition() : Position
    +setPosition(position : Position)
    +getHalfSize() : float
    +addScripts(scripts : Script...) : void
    +removeScript(script : Script) : void
    +executeScripts(other : GameObjectList, input : Input, currentTime : int) : void
    +{abstract} accept(script : GameObjectVisitor) : void
}

abstract class LevelDescription {
    -sections : List<Section>
    -s : Iterator<Section>
    -currentSection Section
    -levelEnd : boolean
    #getSections : List<Section>
    #getLevelEnd : boolean
    +initialize() : void
    +createPlayer(type : ShotType) : Player
    +update(gameObjectList : GameObjectList, currentTime : int) : void
}

Level "1" *-r- "1" GameObjectList
Level "1" *-r- "1" LevelDescription
Level "1" *-l- "*" Manager
Level "1" *-- "*" FixedManager

GameObjectList "1" *-r- "*" GameObject
note on link : Dans le modele interne de la classe GameObjectList,\nil  y en realite plusieurs listes mais l'UML ne represente\nqu'un seul trait pour des questions de lisibilite.\nIl y a aussi une liste des objects qui seront ajoute\net une liste des objets qui seront supprimes a la prochaine frame.

GameObjectVisitor <|.. Manager
Manager <|.. FixedManager
FixedManager <|.. CollisionEngine
FixedManager <|.. MovementEngine
Manager <|.. RenderEngine

@enduml
```

Représente les différentes interactions pour la gestion du level au sein de l'application.
<br/><br/>

## Déploiement de l'application

---
Pour lancer l'application, il faut tour simplement exécuter le fichier jar joint au rendu du projet. Il est donc indispensable de disposer de la JRE afin d'exécuter l'application.

Il est cependant possible de compiler le code de l'application avant de lancer l'application.

## Utilisation de l'application

---
Au lancement de l’application une fenêtre affiche 3 boutons :

- *__Start__* : Pour lancer le jeu
- *__Option__* : Pour définir des options pour le jeu
- *__Quit__* : Pour fermer l’application

![Écran d'accueil de l'application](img/game.png)

On peut appuyer sur le bouton *__Option__*, pour choisir :

- Le niveau : FirstLevel ou SecondLevel
- La difficulté : Facile, Normal ou difficile
- Le type de tir pour notre vaisseau : TypeA, TypeB et TypeC
- La vitesse de jeu de l’application.

![Écran des options de jeu](img/options.png)

On peut appuyer sur le bouton Start pour lancer le jeu en tenant compte des options choisies à la page Option.

- Le vaisseau peut-être déplacé de droite à gauche et de haut en bas avec les boutons de direction
- On utiliser la touche __Y__ pour tirer avec le vaisseau. Les tirs peuvent se faire en continue en gardant la touche **Y** enfoncer.
- On peut appuyer sur le bouton *__SHIFT__* pour ralentir les mouvements de notre joueur.

![Jeu](img/party.png)
