package view;

import lombok.Getter;
import lombok.Setter;
import model.options.Difficulty;
import model.options.LevelSelect;
import model.options.Options;
import model.options.ShotType;
import view.component.ControlsSystem;
import view.scene.AbstractScene;
import view.scene.StartMenuScene;

import javax.swing.*;
import java.util.Stack;

/**
 * Game windows. Its main role is to manage the stack scene.
 * Only the scene at the top of the stack will display on the window.
 */
public final class MainFrame extends JFrame {
    private final Stack<AbstractScene> scenes;

    @Getter
    @Setter
    private Options options; // Contains various game options to load when running the game.

    /**
     * Constructor for the game window. Initialise the windows, the stack scenes and the inputs capture.
     */
    private MainFrame() {
        options = new Options(Difficulty.NORMAL, ShotType.TypeA, 1, LevelSelect.FirstLevel);
        scenes = new Stack<>();

        // Setup window.
        int width = 1000, height = 800;

        setTitle("Game");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(width, height);
        setLocationRelativeTo(null); // Hints graphics toolkit to center the window.
        addScene(new StartMenuScene(this));
        setVisible(true);

        ControlsSystem.Initialise(getRootPane());
    }

    public static void main(String[] args) {
        new MainFrame();
    }

    /**
     * Add a new scene to the stack
     * @param scene AbstractScene
     */
    public void addScene(AbstractScene scene) {
        if (!scenes.isEmpty())
            remove(scenes.peek());

        scenes.add(scene);
        add(scene);
        scene.focusReceived();
        revalidate();
        repaint();
    }

    /**
     * Remove a number of scene off the stack
     * @param popCount int
     */
    public void popScene(int popCount) {
        for (int i = 0; i < popCount; i++) {
            remove(scenes.peek());
            scenes.pop();
        }
        add(scenes.peek());
        scenes.peek().focusReceived();
        revalidate();
        repaint();
    }
}