package view.component;

import javax.swing.*;

/**
 * Print the information relative to the debug message.
 * Concretely it's used to calculate an average value in a given time spend
 * and display this information in the debug message
 * For example the time taken by each engine and the number of bullet of a frame.
 */
public class DebugInformationPrinter extends JLabel {
    private final String messageStart;
    private final String messageEnd;
    private final int cycleCount;

    private long sum;
    private int count;

    public DebugInformationPrinter(int cycleCount, String messageStart, String messageEnd) {
        super(messageStart + 0 + messageEnd);
        this.messageStart = messageStart;
        this.messageEnd = messageEnd;
        this.cycleCount = cycleCount;
    }

    public void setValue(long value) {
        sum += value;
        count++;
        if (count == cycleCount) {
            count = 0;
            setText(messageStart + (sum / cycleCount) + messageEnd);
            sum = 0;
        }
    }
}