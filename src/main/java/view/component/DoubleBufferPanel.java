package view.component;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Intermediary buffer that wait a finished frame before displaying. Otherwise keeps displaying the previous frame.
 */
public class DoubleBufferPanel extends JPanel {
    private BufferedImage next; // Next frame to display.
    private BufferedImage current;

    protected void swapBuffers(BufferedImage newImage) {
        next = newImage;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (next != null) {
            current = next;
            next = null;
        }

        if (current == null) {
            return;
        }

        g.drawImage(current, 0, 0, getWidth(), getHeight(), null);
    }
}