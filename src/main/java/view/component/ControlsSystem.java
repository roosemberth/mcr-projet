package view.component;

import lombok.Getter;
import model.Input;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.EnumSet;
import java.util.Set;

/**
 * Defines the events to capture the user inputs
 */
public final class ControlsSystem {
    // List all the pressed inputs relevant to the game.
    private static final Set<Input.Commands> inputsActive = EnumSet.noneOf(Input.Commands.class);

    @Getter
    private static boolean isGamePaused;

    private ControlsSystem() {
    }

    /**
     * Get the list of input pressed.
     * @return Input
     */
    public static Input getInput() {
        return new Input(inputsActive);
    }

    /**
     * Set the events for the keyboard inputs.
     * @param rootPane JRootPane
     */
    public static void Initialise(JRootPane rootPane) {
        inputsActive.clear();

        InputMap inputs = rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap actions = rootPane.getActionMap();

        // Reset
        inputs.clear();
        actions.clear();

        setControlsOnAndOff(inputs, actions, KeyEvent.VK_UP, Input.Commands.UP);
        setControlsOnAndOff(inputs, actions, KeyEvent.VK_DOWN, Input.Commands.DOWN);
        setControlsOnAndOff(inputs, actions, KeyEvent.VK_RIGHT, Input.Commands.RIGHT);
        setControlsOnAndOff(inputs, actions, KeyEvent.VK_LEFT, Input.Commands.LEFT);
        setControlsOnAndOff(inputs, actions, KeyEvent.VK_SHIFT, Input.Commands.FOCUS);
        setControlsOnAndOff(inputs, actions, KeyEvent.VK_Y, Input.Commands.SHOT);

        inputs.put(KeyStroke.getKeyStroke(
                KeyEvent.VK_ESCAPE, 0, false), "ENTER_PAUSE");
        inputs.put(KeyStroke.getKeyStroke(
                KeyEvent.VK_ESCAPE, InputEvent.SHIFT_DOWN_MASK, false), "ENTER_PAUSE");
        inputs.put(KeyStroke.getKeyStroke(
                KeyEvent.VK_ESCAPE, 0, true), "EXIT_PAUSE");
        inputs.put(KeyStroke.getKeyStroke(
                KeyEvent.VK_ESCAPE, InputEvent.SHIFT_DOWN_MASK, true), "EXIT_PAUSE");

        actions.put("ENTER_PAUSE", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                isGamePaused = true;
            }
        });

        actions.put("EXIT_PAUSE", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                isGamePaused = false;
            }
        });
    }

    /**
     * Defines an event for when a key is pressed and another one for when it's released.
     * @param inputs InputMap
     * @param actions ActionMap
     * @param key int
     * @param action Input.Commands
     */
    private static void setControlsOnAndOff(InputMap inputs, ActionMap actions, int key, Input.Commands action) {
        setControls(inputs, actions, key, false, action.name() + "_ON", action);
        setControls(inputs, actions, key, true, action.name() + "_OFF", action);
    }

    /**
     * Set an event for a key input.
     * @param inputs InputMap
     * @param actions ActionMap
     * @param key int
     * @param isOnKeyRelease boolean
     * @param name String
     * @param action Input.Commands
     */
    private static void setControls(InputMap inputs, ActionMap actions, int key, boolean isOnKeyRelease, String name,
                                    Input.Commands action) {
        inputs.put(KeyStroke.getKeyStroke(key, 0, isOnKeyRelease), name);
        inputs.put(KeyStroke.getKeyStroke(key, InputEvent.SHIFT_DOWN_MASK, isOnKeyRelease), name); // Ignore cap change
        actions.put(name, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isOnKeyRelease) {
                    inputsActive.remove(action);
                } else {
                    inputsActive.add(action);
                }
            }
        });
    }
}
