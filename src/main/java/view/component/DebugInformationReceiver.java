package view.component;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Play as an intermediary between the model and the view to carry game information to the display.
 * Implements debugInformationReceiver from the model.
 */
public class DebugInformationReceiver implements model.DebugInformationReceiver {
    private final JPanel panel;
    private final Map<String, DebugInformationPrinter> printers = new HashMap<>();

    public DebugInformationReceiver(JPanel panel) {
        this.panel = panel;
    }

    @Override
    public void setManagerTime(String managerName, long time) {
        if (printers.containsKey(managerName)) {
            printers.get(managerName).setValue(time);
        } else {
            DebugInformationPrinter printer =
                    new DebugInformationPrinter(60, "        " + managerName + " : ", " us");
            printer.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel.add(printer);
            printers.put(managerName, printer);
        }
    }
}