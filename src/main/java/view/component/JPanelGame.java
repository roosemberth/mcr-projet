package view.component;

import view.render.RenderEngine;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.EnumMap;

/**
 * JPanel used to display the game canvas.
 * Contains multiple layers of buffered image in order to display some object on top of others.
 * A layer contains a series of individual objects.
 * It creates a priority that allows some object to always be display on top of others. See RenderEngine for details.
 */
public class JPanelGame extends DoubleBufferPanel {
    private final EnumMap<RenderEngine.CanvasLayers, BufferedImage> layersImages =
            new EnumMap<>(RenderEngine.CanvasLayers.class);

    private final EnumMap<RenderEngine.CanvasLayers, Graphics2D> layersGraphics =
            new EnumMap<>(RenderEngine.CanvasLayers.class);

    /**
     * Get the appropriate layer based on the canvas in parameter.
     * @param layers Render.Engine.CanvasLayers.
     * @return Graphics2D
     */
    public Graphics2D getGraphics(RenderEngine.CanvasLayers layers) {
        if (!layersGraphics.containsKey(layers)) {
            BufferedImage bi = new BufferedImage(getWidth(), getHeight(), RenderEngine.imageType);
            layersImages.put(layers, bi);
            layersGraphics.put(layers, bi.createGraphics());
        }
        return layersGraphics.get(layers);
    }

    /**
     * Display a frame step by step by displaying a background (currently a gray rectangle....) and the
     * following layers. The layers are currently in number of 3. See the RenderEngine for details.
     */
    public void validate() {
        BufferedImage buffer = new BufferedImage(getWidth(), getHeight(), RenderEngine.imageType);
        Graphics2D g = buffer.createGraphics();

        g.setColor(Color.gray);
        g.fillRect(0, 0, getWidth(), getHeight());

        for (RenderEngine.CanvasLayers fg : RenderEngine.CanvasLayers.values()) {
            if (layersImages.containsKey(fg)) {
                g.drawImage(layersImages.get(fg), 0, 0, getWidth(), getHeight(), null);
            }
        }

        swapBuffers(buffer);
    }

    /**
     * Empty all layers.
     */
    public void clear() {
        layersImages.clear();
        layersGraphics.clear();
    }
}
