package view.scene;

import lombok.AccessLevel;
import lombok.Getter;
import view.MainFrame;

import javax.swing.*;

/**
 * Define the functionalities that a scene must implement.
 * It's attached to the main window.
 * A scene can display game menu, options, pause or even the game itself.
 */
public abstract class AbstractScene extends JPanel {
    @Getter(AccessLevel.PROTECTED)
    private final MainFrame mainFrame;

    protected AbstractScene(MainFrame frame) {
        mainFrame = frame;
    }

    public abstract void focusReceived();
}