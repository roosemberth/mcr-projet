package view.scene;

import model.options.Difficulty;
import model.options.LevelSelect;
import model.options.Options;
import model.options.ShotType;
import view.MainFrame;

import javax.swing.*;
import java.awt.*;

/**
 * Option menu. Here the user can change various game options.
 * The following can be modify : The level select (Currently the only way to go to the next level), the shot type,
 * the difficulty and the game speed.
 */
public class OptionScene extends AbstractScene {
    private JToggleButton selectedDifficulty;
    private JToggleButton selectedShotType;
    private JToggleButton selectedLevel;

    /**
     * Constructor
     * @param frame MainFrame
     */
    public OptionScene(MainFrame frame) {
        super(frame);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        add(Box.createVerticalGlue());

        JLabel label = new JLabel("Options");
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(label);

        optionLevel();
        optionDifficulty();
        optionShotType();
        optionSpeed();

        JButton back = new JButton("back");
        back.addActionListener(e -> getMainFrame().popScene(1));
        back.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(back);

        add(Box.createVerticalGlue());
    }

    @Override
    public void focusReceived() {

    }

    private Options getOptions() {
        return getMainFrame().getOptions();
    }

    private void setOptions(Options options) {
        getMainFrame().setOptions(options);
    }

    /**
     * Allows to select a level when starting the game.
     */
    private void optionLevel() {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel.add(new JLabel("Level"));

        for (LevelSelect type : LevelSelect.values()) {
            JToggleButton button = new JToggleButton(type.name());
            if (getOptions().getLevelSelected() == type) {
                selectedLevel = button;
                button.setSelected(true);
            }
            button.addActionListener(e -> {
                setOptions(getOptions().withLevelSelected(type));
                selectedLevel.setSelected(false);
                selectedLevel = button;
            });
            panel.add(button);
        }
        add(panel);
    }

    /**
     * Change the difficulty between easy, normal and hard.
     */
    private void optionDifficulty() {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel.add(new JLabel("Difficulty"));

        for (Difficulty type : Difficulty.values()) {
            JToggleButton button = new JToggleButton(type.name());
            if (getOptions().getDifficulty() == type) {
                selectedDifficulty = button;
                button.setSelected(true);
            }
            button.addActionListener(e -> {
                setOptions(getOptions().withDifficulty(type));
                selectedDifficulty.setSelected(false);
                selectedDifficulty = button;
            });
            panel.add(button);
        }

        add(panel);
    }

    /**
     * Select a shot type. There are 3 shot types named A, B and C.
     * It affects the pattern in which the player shots.
     */
    private void optionShotType() {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel.add(new JLabel("ShotType"));

        for (ShotType type : ShotType.values()) {
            JToggleButton button = new JToggleButton(type.name());
            if (getOptions().getShotType() == type) {
                selectedShotType = button;
                button.setSelected(true);
            }
            button.addActionListener(e -> {
                setOptions(getOptions().withShotType(type));
                selectedShotType.setSelected(false);
                selectedShotType = button;
            });
            panel.add(button);
        }
        add(panel);
    }

    /**
     * Adjusts the game ticking speed between 0.5 and 2. By default the game runs 4 ticks by frame.
     */
    private void optionSpeed() {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel.add(new JLabel("Speed"));

        JSlider slider = new JSlider(SwingConstants.HORIZONTAL, 50, 200, (int) (getOptions().getGameSpeed() * 100));
        JLabel value = new JLabel(String.valueOf(getOptions().getGameSpeed()));
        slider.addChangeListener(e -> {
            setOptions(getOptions().withGameSpeed(slider.getValue() / 100.0F));
            value.setText(String.valueOf(slider.getValue() / 100.0F));
        });
        panel.add(slider);
        panel.add(value);
        add(panel);
    }
}