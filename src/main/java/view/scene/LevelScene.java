package view.scene;

import model.DebugInformationReceiver;
import model.Level;
import view.MainFrame;
import view.component.ControlsSystem;
import view.component.DebugInformationPrinter;
import view.component.JPanelGame;
import view.render.RenderEngine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.TimerTask;

/**
 * Level scene. Display the game and the debug menu. Manage the game.
 * Calls various engines and managers to update the game.
 * Manage the refreshing and ticking.
 */
public class LevelScene extends AbstractScene {
    private static final boolean showDebugInformations = true;
    private final JPanelGame mainPanel;
    private final RenderEngine renderEngine;
    private final JPanel infoPanel;
    private final Level level;
    private final DebugInformationPrinter totalFrameTime;
    private final DebugInformationPrinter renderTimeDebug;
    private final DebugInformationPrinter physicTimeDebug;
    private final DebugInformationPrinter bulletCount;
    private final DebugInformationReceiver debugInformationReceiver;

    private long lastFrameStart;
    private java.util.Timer timer;

    /**
     * Constructor
     * @param frame MainFrame
     */
    public LevelScene(MainFrame frame) {
        super(frame);

        addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent e) {
                isResized();
            }

            @Override
            public void componentMoved(ComponentEvent e) {
            }

            @Override
            public void componentShown(ComponentEvent e) {
            }

            @Override
            public void componentHidden(ComponentEvent e) {
            }
        });

        setLayout(null);

        mainPanel = new JPanelGame();
        mainPanel.setBounds(0, 0, 0, 0);
        add(mainPanel);
        renderEngine = new RenderEngine(mainPanel);

        infoPanel = new JPanel();
        infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
        infoPanel.setBounds(0, 0, 0, 0);
        add(infoPanel);

        // Initialise the display for the info debug. Displays the time taken by each system parts.
        if (showDebugInformations) {
            totalFrameTime = new DebugInformationPrinter(60, "Total time : ", " us");
            physicTimeDebug = new DebugInformationPrinter(60, "    Time logic : ", " us");
            renderTimeDebug = new DebugInformationPrinter(60, "    Time render : ", " us");
            bulletCount = new DebugInformationPrinter(60, "Bullets : ", "");
            totalFrameTime.setAlignmentX(Component.LEFT_ALIGNMENT);
            physicTimeDebug.setAlignmentX(Component.LEFT_ALIGNMENT);
            renderTimeDebug.setAlignmentX(Component.LEFT_ALIGNMENT);
            bulletCount.setAlignmentX(Component.LEFT_ALIGNMENT);
            infoPanel.add(bulletCount);
            infoPanel.add(totalFrameTime);
            infoPanel.add(renderTimeDebug);
            infoPanel.add(physicTimeDebug);
            debugInformationReceiver = new view.component.DebugInformationReceiver(infoPanel);
        } else {
            debugInformationReceiver = new DebugInformationReceiver() {
            };
        }

        level = new Level(getMainFrame().getOptions());
        level.addDynamicManager(renderEngine);
    }

    @Override
    public void focusReceived() {
        timer = new java.util.Timer();

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                update();
            }
        };

        timer.scheduleAtFixedRate(task, 100, 16); // 16ms for ~60 fps.
        lastFrameStart = System.nanoTime() + 100000000;
        // A value of 100000000 nanoseconds is used to compensate for the 100 millisecond delay before starting the timer
    }

    /**
     * Update the debug display
     * @param totalTime long
     * @param physicTime long
     * @param renderTime long
     * @param bulletCount int
     */
    private void debugUpdate(long totalTime, long physicTime, long renderTime, int bulletCount) {
        totalFrameTime.setValue(totalTime / 1000);
        physicTimeDebug.setValue(physicTime / 1000);
        renderTimeDebug.setValue(renderTime / 1000);
        this.bulletCount.setValue(bulletCount);
    }

    /**
     * Allows to resize the game and keep the proportion if the window is resized.
     */
    private void isResized() {
        // Ratio between game engine and viewport size.
        final int debugPanelSize = 200;
        if (getHeight() > 0 && getWidth() > debugPanelSize) {
            float ratio = Math.min((getHeight() - 10) / Level.SIMULATION_HEIGHT, (getWidth() - debugPanelSize - 15) / Level.SIMULATION_WIDTH);

            int widthPanel = (int) (Level.SIMULATION_WIDTH * ratio);
            int heightPanel = (int) (Level.SIMULATION_HEIGHT * ratio);

            int heightDecal = (getHeight() - heightPanel) / 2;
            int widthDecal = (getWidth() - widthPanel - debugPanelSize - 5) / 2;

            mainPanel.setBounds(widthDecal, heightDecal, widthPanel, heightPanel);
            infoPanel.setBounds(widthDecal + 5 + widthPanel, heightDecal, debugPanelSize, heightPanel);

            renderEngine.setRatio(ratio);
        } else {
            mainPanel.setBounds(0, 0, 0, 0);
            infoPanel.setBounds(0, 0, 0, 0);
            renderEngine.setRatio(-1);
        }
        revalidate();
        repaint();
    }

    /**
     * Manage the pause action.
     * Updates the game logic then updates the frame.
     */
    public void update() {
        long frameStart = System.nanoTime();

        if (ControlsSystem.isGamePaused()) {
            pause();
        } else {
            mainPanel.clear();

            Level.GameState gameState = level.update(ControlsSystem.getInput(), frameStart - lastFrameStart, debugInformationReceiver);

            if (gameState == Level.GameState.inProgress) {
                long physicTime = System.nanoTime();
                mainPanel.validate();
                mainPanel.repaint();
                long renderTime = System.nanoTime();

                if (showDebugInformations) {
                    debugUpdate(
                            renderTime - frameStart,
                            physicTime - frameStart,
                            renderTime - physicTime,
                            level.getBulletCount());
                }

                lastFrameStart = frameStart;
            } else {
                timer.cancel();
                timer = null;
                getMainFrame().addScene(new ResultMenuScene(getMainFrame(), gameState == Level.GameState.won));
            }
        }
    }

    /**
     * Calls to the pause menu. Stop the timer from ticking.
     */
    private void pause() {
        timer.cancel();
        timer = null;
        getMainFrame().addScene(new PauseMenuScene(getMainFrame()));
    }
}