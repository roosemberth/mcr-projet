package view.scene;

import view.MainFrame;

import javax.swing.*;
import java.awt.*;

/**
 * Display the start game menu.
 * When launching the application this is the first scene on the stack.
 * From here we can access the game (Start), the options or quit.
 */
public class StartMenuScene extends AbstractScene {
    public StartMenuScene(MainFrame frame) {
        super(frame);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(Box.createVerticalGlue());
        addChoice("Start", this::startGame);
        add(Box.createRigidArea(new Dimension(15, 15)));
        addChoice("Option", this::goToOption);
        add(Box.createRigidArea(new Dimension(15, 15)));
        addChoice("Quit", this::quit);
        add(Box.createVerticalGlue());
    }

    @Override
    public void focusReceived() {

    }

    /**
     * Create a button and apply an action when clicked.
     * @param text String
     * @param action Runnable
     */
    private void addChoice(String text, Runnable action) {
        JButton option = new JButton(text);
        option.addActionListener(e -> action.run());
        option.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(option);
    }

    /**
     * Load the options scene.
     */
    private void goToOption() {
        getMainFrame().addScene(new OptionScene(getMainFrame()));
    }

    /**
     * Launch the game.
     */
    private void startGame() {
        getMainFrame().addScene(new LevelScene(getMainFrame()));
    }

    /**
     * Exit the app.
     */
    private void quit() {
        System.exit(0);
    }
}