package view.scene;

import view.MainFrame;

import javax.swing.*;
import java.awt.*;

/**
 * Pause game menu. It's called from the LevelScene and when resuming it comes back to it.
 */
public class PauseMenuScene extends AbstractScene {
    /**
     * Constructor
     * @param frame MainFrame
     */
    public PauseMenuScene(MainFrame frame) {
        super(frame);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(Box.createVerticalGlue());
        addChoice("Resume", this::resumeLevel);
        add(Box.createRigidArea(new Dimension(15, 15)));
        addChoice("Restart", this::restartLevel);
        add(Box.createRigidArea(new Dimension(15, 15)));
        addChoice("Quit level", this::quitLevel);
        add(Box.createVerticalGlue());
    }

    @Override
    public void focusReceived() {

    }

    /**
     * Create a button and apply an action when clicked.
     * @param text String
     * @param action Runnable
     */
    private void addChoice(String text, Runnable action) {
        JButton option = new JButton(text);
        option.addActionListener(e -> action.run());
        option.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(option);
    }

    /**
     * Comes back to the LevelScene.
     */
    private void resumeLevel() {
        getMainFrame().popScene(1);
    }

    /**
     * Delete previous game and load a new one.
     */
    private void restartLevel() {
        getMainFrame().popScene(2);
        getMainFrame().addScene(new LevelScene(getMainFrame()));
    }

    /**
     * Goes back to the start menu scene.
     */
    private void quitLevel() {
        getMainFrame().popScene(2);
    }
}