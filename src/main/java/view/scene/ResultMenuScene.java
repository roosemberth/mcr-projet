package view.scene;

import view.MainFrame;

import javax.swing.*;
import java.awt.*;

/**
 * Game result scene.
 * It's displayed whenever there is a game over for if the player win or lose.
 */
public class ResultMenuScene extends AbstractScene {
    /**
     * Display a message saying YOU WIN or YOU LOSE base on the game result.
     * This constructor is call from LevelScene when the game ends.
     * @param frame MainFrame
     * @param isWin boolean
     */
    protected ResultMenuScene(MainFrame frame, boolean isWin) {
        super(frame);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(Box.createVerticalGlue());
        JLabel info = new JLabel(isWin ? "YOU WIN" : "YOU LOSE");
        JButton mainMenu = new JButton("Return to main menu");
        mainMenu.addActionListener(e -> getMainFrame().popScene(2));
        mainMenu.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(info);
        add(mainMenu);
        add(Box.createVerticalGlue());
    }

    @Override
    public void focusReceived() {

    }
}
