package view.render.sprite;

import java.awt.*;

public class BulletEnemySprite extends AbstractSprite {
    public BulletEnemySprite(int size) {
        super(size);

        Graphics2D g = getImage().createGraphics();

        drawCircleAtCenter(g, size, Color.PINK);

        g.dispose();
    }
}