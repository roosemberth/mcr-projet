package view.render.sprite;

import java.awt.*;

public class PlayerSprite extends AbstractSprite {
    public PlayerSprite(int size) {
        super(size * 3);
        Graphics2D g = getImage().createGraphics();

        drawCircleAtCenter(g, size * 3, Color.BLUE);
        drawCircleAtCenter(g, size, Color.WHITE);

        g.dispose();
    }
}