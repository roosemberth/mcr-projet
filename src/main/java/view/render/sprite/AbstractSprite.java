package view.render.sprite;

import lombok.Getter;
import view.render.RenderEngine;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 * Defines a sprite to be used to display a game object.
 */
@Getter
public abstract class AbstractSprite {
    private final BufferedImage image;
    private final int size;

    /**
     * Constructor.
     * A sprite is draw inside a bufferedImage. The same image can be used for multiple object of the same class.
     * @param size int
     */
    protected AbstractSprite(int size) {
        image = new BufferedImage(size, size, RenderEngine.imageType);
        this.size = size;
    }

    /**
     * Draw a circle centered based on the full size. Add a black border to the shape.
     * @param g Graphics2D
     * @param sizeShape int
     * @param color Color
     */
    protected void drawCircleAtCenter(Graphics2D g, int sizeShape, Color color) {
        float borderWidth = sizeShape * 0.1f;
        int offsetSize = (int) (sizeShape - borderWidth * 2);
        int offsetToCenter = (size - offsetSize) / 2;
        Shape shape = new Ellipse2D.Float(offsetToCenter, offsetToCenter, offsetSize, offsetSize);
        g.setColor(color);
        g.fill(shape);

        // outline
        g.setColor(Color.black);
        g.setStroke(new BasicStroke(borderWidth));
        g.draw(shape);
    }

    /**
     * Draw a square centered based on the full size. Add a black border to the shape.
     * @param g Graphics2D
     * @param sizeShape int
     * @param color Color
     */
    protected void drawSquareAtCenter(Graphics2D g, int sizeShape, Color color) {
        float borderWidth = sizeShape * 0.1f;
        int offsetSize = (int) (sizeShape - borderWidth * 2);
        int offsetToCenter = (size - offsetSize) / 2;
        Shape shape = new Rectangle2D.Float(offsetToCenter, offsetToCenter, offsetSize, offsetSize);
        g.setColor(color);
        g.fill(shape);

        // outline
        g.setColor(Color.black);
        g.setStroke(new BasicStroke(borderWidth));
        g.draw(shape);
    }

    /**
     * Draw a cercle at the center but does not draw a border.
     * @param g Graphics2D
     * @param offsetSize int
     * @param color Color
     */
    protected void drawCircleAtCenterNoBorder(Graphics2D g, int offsetSize, Color color) {
        int offsetToCenter = (size - offsetSize) / 2;
        Shape shape = new Ellipse2D.Float(offsetToCenter, offsetToCenter, offsetSize, offsetSize);
        g.setColor(color);
        g.fill(shape);

        g.draw(shape);
    }
}