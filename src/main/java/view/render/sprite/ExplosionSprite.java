package view.render.sprite;

import java.awt.*;

public class ExplosionSprite extends AbstractSprite {
    public ExplosionSprite(int size) {
        super(size);

        Graphics2D g = getImage().createGraphics();

        drawCircleAtCenterNoBorder(g, size, new Color(Color.ORANGE.getRed(), Color.ORANGE.getGreen(), Color.ORANGE.getBlue(), 100));
        drawCircleAtCenterNoBorder(g, size / 2, new Color(Color.YELLOW.getRed(), Color.YELLOW.getGreen(), Color.YELLOW.getBlue(), 100));

        g.dispose();
    }
}