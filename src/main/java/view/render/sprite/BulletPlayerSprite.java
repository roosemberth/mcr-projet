package view.render.sprite;

import java.awt.*;

public class BulletPlayerSprite extends AbstractSprite {
    public BulletPlayerSprite(int size) {
        super(size);

        Graphics2D g = getImage().createGraphics();

        drawCircleAtCenterNoBorder(g, size, new Color(Color.YELLOW.getRed(), Color.YELLOW.getGreen(), Color.YELLOW.getBlue(), 50));

        g.dispose();
    }
}