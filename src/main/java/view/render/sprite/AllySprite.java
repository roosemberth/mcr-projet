package view.render.sprite;

import java.awt.*;

public class AllySprite extends AbstractSprite {
    public AllySprite(int size) {
        super(size);

        Graphics2D g = getImage().createGraphics();

        drawCircleAtCenter(g, size, Color.BLUE);

        g.dispose();
    }
}
