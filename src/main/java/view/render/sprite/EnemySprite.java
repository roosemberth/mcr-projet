package view.render.sprite;

import java.awt.*;

public class EnemySprite extends AbstractSprite {
    public EnemySprite(int size) {
        super(size);

        Graphics2D g = getImage().createGraphics();

        drawCircleAtCenter(g, size, Color.RED);

        g.dispose();
    }
}