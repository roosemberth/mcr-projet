package view.render.sprite;

import java.awt.*;

public class ItemSprite extends AbstractSprite {

    public ItemSprite(int size) {
        super(size);
        Graphics2D g = getImage().createGraphics();

        drawSquareAtCenter(g, size, new Color(Color.GREEN.getRed(), Color.GREEN.getGreen(), Color.GREEN.getBlue(), 100));

        g.dispose();
    }
}