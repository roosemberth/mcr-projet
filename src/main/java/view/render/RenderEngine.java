package view.render;

import model.Level;
import model.gameObjects.*;
import model.geometry.Position;
import model.managers.Manager;
import view.component.JPanelGame;
import view.render.sprite.*;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * RenderEngine. Manage to display the sprite of game objects.
 * It inherits from Manager which itself inherits GameObjectVisitor which allows us to display a sprite by object basis.
 * The object are unaware to how they are displayed.
 */
public class RenderEngine implements Manager {
    public static final int imageType = BufferedImage.TYPE_INT_ARGB_PRE;
    private final JPanelGame panel;
    private float ratio;
    private AbstractSprite playerSprite;
    private AbstractSprite enemySprite;
    private AbstractSprite playerBulletSprite;
    private AbstractSprite enemyBulletSprite;
    private AbstractSprite itemSprite;
    private AbstractSprite explosionSprite;
    private AbstractSprite allySprite;

    /**
     * Constructor
     * @param p JPanelGame
     */
    public RenderEngine(JPanelGame p) {
        panel = p;
        setRatio(-1);
    }

    /**
     * Display the sprite to the position on the graphics object.
     * @param sprite AbstractSprite
     * @param pos Position
     * @param g Graphics2D
     */
    private void display(AbstractSprite sprite, Position pos, Graphics2D g) {
        if (ratio != -1) {
            float offset = sprite.getSize() / 2.0F;
            float x = pos.getX() * ratio - offset;
            float y = (Level.SIMULATION_HEIGHT - pos.getY()) * ratio - offset;
            g.drawImage(sprite.getImage(), (int) x, (int) y, sprite.getSize(), sprite.getSize(), null);
        }
    }

    /**
     * Defines the sprite size based on the game logic and the window size.
     * @param ratio float
     */
    public void setRatio(float ratio) {
        if (ratio != -1) {
            playerSprite = new PlayerSprite((int) (Level.PlayerSize * ratio));
            enemySprite = new EnemySprite((int) (Level.EnemySize * ratio));
            playerBulletSprite = new BulletPlayerSprite((int) (Level.PlayerBulletSize * ratio));
            enemyBulletSprite = new BulletEnemySprite((int) (Level.EnemyBulletSize * ratio));
            itemSprite = new ItemSprite((int) (Level.ItemSize * ratio));
            explosionSprite = new ExplosionSprite((int) (Level.ExplosionSize * ratio));
            allySprite = new AllySprite((int) (Level.AllySize * ratio));
        }
        this.ratio = ratio;
    }

    @Override
    public void visit(Player o) {
        display(playerSprite, o.getPosition(), panel.getGraphics(CanvasLayers.BACK));
    }

    @Override
    public void visit(Enemy o) {
        display(enemySprite, o.getPosition(), panel.getGraphics(CanvasLayers.FRONT));
    }

    @Override
    public void visit(EnemyBullet o) {
        display(enemyBulletSprite, o.getPosition(), panel.getGraphics(CanvasLayers.MIDDLE));
    }

    @Override
    public void visit(PlayerBullet o) {
        display(playerBulletSprite, o.getPosition(), panel.getGraphics(CanvasLayers.BACK));
    }

    @Override
    public void visit(Item o) {
        display(itemSprite, o.getPosition(), panel.getGraphics(CanvasLayers.BACK));
    }

    @Override
    public void visit(Decoration o) {
        display(explosionSprite, o.getPosition(), panel.getGraphics(CanvasLayers.BACK));
    }

    @Override
    public void visit(Ally o) {
        display(allySprite, o.getPosition(), panel.getGraphics(CanvasLayers.BACK));
    }

    @Override
    public String getName() {
        return "Render Engine";
    }

    /**
     * Priority of displayed object. Object on the Front will always be on top of object in the middle and the back.
     */
    public enum CanvasLayers {BACK, MIDDLE, FRONT}
}
