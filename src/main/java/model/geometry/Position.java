package model.geometry;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Define a point or coordinate on the game field.
 */
@Data
@AllArgsConstructor
public final class Position {
    private final float x, y;

    public Position withX(float newX) {
        return new Position(newX, y);
    }

    public Position withY(float newY) {
        return new Position(x, newY);
    }

    public Position add(float x, float y) {
        return new Position(this.x + x, this.y + y);
    }

    public Position add(Vector v) {
        return add(v.getX(), v.getY());
    }
}