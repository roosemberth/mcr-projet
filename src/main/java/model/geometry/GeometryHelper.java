package model.geometry;

/**
 * Class use to help make common geometric calculation for the Path Object maybe some scripts.
 */
public final class GeometryHelper {
    private GeometryHelper() {
    }

    /**
     * Get a vector based on an angle and a distance (speed).
     * @param angle float
     * @param speed float
     * @return Vector
     */
    public static Vector getVectorFromAngleAndSpeed(float angle, float speed) {
        return new Vector((float) Math.cos(Math.toRadians(angle)) * speed, (float) Math.sin(Math.toRadians(angle)) * speed);
    }

    /**
     * Get an angle corresponding to the vector given.
     * @param v Vector
     * @return float
     */
    public static float getAngleFromVector(Vector v) {
        return (float) (Math.atan2(v.getY(), v.getX()) * 180 / Math.PI);
    }
}