package model.geometry;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Define a direction unlike Position that define a point.
 */
@Data
@AllArgsConstructor
public class Vector {
    private final float x, y;

    public Vector(Position first, Position second) {
        x = second.getX() - first.getX();
        y = second.getY() - first.getY();
    }

    public Vector(Vector first, Vector second) {
        x = second.getX() - first.getX();
        y = second.getY() - first.getY();
    }

    public Vector resized(float newSize) {
        return multiply(newSize / lenght());
    }

    public Vector normalized() {
        return divide(lenght());
    }

    public float lenght() {
        return (float) Math.sqrt(squareLenght());
    }

    public float squareLenght() {
        return x * x + y * y;
    }

    public Vector multiply(float factor) {
        return new Vector(x * factor, y * factor);
    }

    public Vector divide(float factor) {
        return new Vector(x / factor, y / factor);
    }

    public Vector withX(float newX) {
        return new Vector(newX, y);
    }

    public Vector withY(float newY) {
        return new Vector(x, newY);
    }

    public Vector add(float x, float y) {
        return new Vector(this.x + x, this.y + y);
    }

    public Vector add(Vector v) {
        return new Vector(x + v.x, y + v.y);
    }
}
