package model.gameObjects;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import model.Input;
import model.geometry.Position;
import model.managers.GameObjectVisitor;
import model.script.Script;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * Define a GameObject. A GameObject is any object display on the game.
 * Currently : player, enemies, allies, explosion, bullets and items.
 */
@AllArgsConstructor
public abstract class GameObject {
    private final LinkedList<Script> scripts = new LinkedList<>();

    @Getter
    private final float halfSize;

    @Getter
    @Setter
    private Position position;

    public abstract void accept(GameObjectVisitor script);

    public void addScripts(Script... scripts) {
        this.scripts.addAll(Arrays.asList(scripts));
    }

    public void executeScripts(GameObjectList other, Input input, int currentTime) {
        for (Script script : scripts) {
            script.execute(this, other, input, currentTime);
        }
    }

    public void removeScript(Script script) {
        scripts.remove(script);
    }
}