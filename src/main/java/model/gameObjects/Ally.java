package model.gameObjects;

import lombok.Getter;
import model.Level;
import model.commandMovement.CommandMovement;
import model.managers.GameObjectVisitor;

/**
 * Define an object around the player helping them shoot to the enemy.
 */
public class Ally extends CommandObject {
    @Getter
    private final Player player;

    @Getter
    private final float angleShot;

    @Getter
    private final float angleShotFocus;

    public Ally(Player p, float angleShot, float angleShotFocus, CommandMovement path) {
        super(Level.AllySize / 2, p.getPosition(), path);
        this.angleShot = angleShot;
        this.angleShotFocus = angleShotFocus;
        player = p;
    }

    @Override
    public void accept(GameObjectVisitor script) {
        script.visit(this);
    }
}
