package model.gameObjects;

import model.geometry.Position;
import model.managers.GameObjectVisitor;
import model.path.AimedPath;
import model.path.LinearPath;

/**
 * Define an item drop by enemy and rewarding the player when they collect it
 */
public abstract class Item extends PathObject {
    protected Item(Position position, float halfSize) {
        super(position, halfSize, new LinearPath(0, -1));
    }

    /**
     * Reward to give to the player.
     * @param p Player
     */
    public abstract void reward(Player p);

    @Override
    public void accept(GameObjectVisitor script) {
        script.visit(this);
    }

    public void attractByPlayer(Player p) {
        setPath(new AimedPath(p, 3));
    }
}