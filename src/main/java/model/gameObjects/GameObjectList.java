package model.gameObjects;

import model.managers.GameObjectVisitor;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * Contains all game objects currently in the game.
 * Organized them for better access to some types of objects.
 * Use 2 GameObjectVisitor in order to add and remove object in the correct list.
 */
public class GameObjectList implements Iterable<GameObject> {
    private final Collection<GameObject> toAdd = new LinkedList<>(); // list of object to add in the next tick.
    private final Collection<GameObject> toRemove = new LinkedList<>(); // list of object to remove in the next tick.

    private final Player player;
    private final Collection<Enemy> ennemies = new LinkedList<>();
    private final Collection<Bullet> bullets = new LinkedList<>();
    private final Collection<Item> items = new LinkedList<>();
    private final Collection<GameObject> others = new LinkedList<>();

    public GameObjectList(Player player) {
        this.player = player;
    }

    /**
     * Add an object to the appropriate list.
     * @param go GameObject
     */
    public void add(GameObject go) {
        toAdd.add(go);
    }

    /**
     * Remove an object to the appropriate list.
     * @param go GameObject
     */
    public void remove(GameObject go) {
        toRemove.add(go);
    }

    /**
     *
     */
    public void update() {
        GameObjectVisitor remover = new GameObjectVisitor() {
            @Override
            public void visit(Player o) {
                throw new RuntimeException("Player should never be removed");
            }

            @Override
            public void visit(Enemy o) {
                ennemies.remove(o);
            }

            @Override
            public void visit(EnemyBullet o) {
                bullets.remove(o);
            }

            @Override
            public void visit(PlayerBullet o) {
                bullets.remove(o);
            }

            @Override
            public void visit(Item o) {
                items.remove(o);
            }

            @Override
            public void visit(Decoration o) {
                others.remove(o);
            }

            @Override
            public void visit(Ally o) {
                others.remove(o);
            }
        };

        for (GameObject removed : toRemove) {
            removed.accept(remover);
        }

        GameObjectVisitor adder = new GameObjectVisitor() {
            @Override
            public void visit(Player o) {
                throw new RuntimeException("A player can't be added to scene during run");
            }

            @Override
            public void visit(Enemy o) {
                ennemies.add(o);
            }

            @Override
            public void visit(EnemyBullet o) {
                bullets.add(o);
            }

            @Override
            public void visit(PlayerBullet o) {
                bullets.add(o);
            }

            @Override
            public void visit(Item o) {
                items.add(o);
            }

            @Override
            public void visit(Decoration o) {
                others.add(o);
            }

            @Override
            public void visit(Ally o) {
                others.add(o);
            }
        };

        for (GameObject o : toAdd) {
            o.accept(adder);
        }

        toRemove.clear();
        toAdd.clear();
    }

    public int bulletCount() {
        return bullets.size();
    }

    public Iterable<Enemy> enemies() {
        return ennemies;
    }

    public Iterable<Item> items() {
        return items;
    }

    public Player player() {
        return player;
    }

    /**
     * Iterator to go through a particular list of objects.
     * @return
     */
    @Override
    public Iterator<GameObject> iterator() {
        return new Iterator<>() {
            private final Iterator<Enemy> ennemyIterator = ennemies.iterator();
            private final Iterator<Bullet> bulletIterator = bullets.iterator();
            private final Iterator<Item> itemIterator = items.iterator();
            private final Iterator<GameObject> otherIterator = others.iterator();
            private boolean hasNotReturnPlayer = true;

            @Override
            public boolean hasNext() {
                return hasNotReturnPlayer || ennemyIterator.hasNext() || bulletIterator.hasNext()
                        || itemIterator.hasNext() || otherIterator.hasNext();
            }

            @Override
            public GameObject next() {
                if (bulletIterator.hasNext()) {
                    return bulletIterator.next();
                }

                if (ennemyIterator.hasNext()) {
                    return ennemyIterator.next();
                }

                if (itemIterator.hasNext()) {
                    return itemIterator.next();
                }

                if (otherIterator.hasNext()) {
                    return otherIterator.next();
                }

                if (hasNotReturnPlayer) {
                    hasNotReturnPlayer = false;
                    return player;
                }
                throw new NoSuchElementException();
            }
        };
    }
}