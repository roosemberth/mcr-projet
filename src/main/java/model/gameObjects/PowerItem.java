package model.gameObjects;

import model.geometry.Position;

/**
 * Define an item than increase the power point which in turn can level up the shot of the player.
 */
public class PowerItem extends Item {
    public PowerItem(Position position, float halfSize) {
        super(position, halfSize);
    }

    @Override
    public void reward(Player p) {
        p.setPower(p.getPower() + 1);
    }
}