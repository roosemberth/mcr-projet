package model.gameObjects;

import model.geometry.Position;
import model.managers.GameObjectVisitor;
import model.path.Path;

/**
 * Define an object having no interaction with anything but nonetheless is displayed.
 * Example : Currently use for the explosions.
 */
public class Decoration extends PathObject {
    public Decoration(Position position, float halfSize, Path path) {
        super(position, halfSize, path);
    }

    @Override
    public void accept(GameObjectVisitor script) {
        script.visit(this);
    }
}