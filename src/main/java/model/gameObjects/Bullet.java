package model.gameObjects;

import lombok.Getter;
import model.geometry.Position;
import model.path.Path;

/**
 * Define a projectile following a path.
 */
public abstract class Bullet extends PathObject {
    @Getter
    private final int damage;

    protected Bullet(Position position, float halfSize, int damage, Path path) {
        super(position, halfSize, path);
        this.damage = damage;
    }
}