package model.gameObjects;

import model.geometry.Position;
import model.managers.GameObjectVisitor;
import model.path.Path;

/**
 * Define a projectile created by an enemy. It can hit a player.
 */
public class EnemyBullet extends Bullet {
    public EnemyBullet(Position position, float halfSize, Path path, int damage) {
        super(position, halfSize, damage, path);
    }

    @Override
    public void accept(GameObjectVisitor script) {
        script.visit(this);
    }
}