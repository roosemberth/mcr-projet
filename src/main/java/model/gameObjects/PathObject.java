package model.gameObjects;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import model.geometry.Position;
import model.path.Path;

/**
 * Define an object following a path.
 * Almost all game object implement this except Player and Ally which use CommandObject.
 */
public abstract class PathObject extends GameObject {
    @Getter
    @Setter(AccessLevel.PROTECTED)
    private Path path;

    protected PathObject(Position position, float halfSize, Path path) {
        super(halfSize, position);
        this.path = path;
    }
}