package model.gameObjects;

import lombok.Getter;
import lombok.Setter;
import model.commandMovement.PlayerMovement;
import model.geometry.Position;
import model.managers.GameObjectVisitor;

/**
 * Define an object controlled by the player.
 */
public class Player extends CommandObject {
    @Getter
    private final float speed;

    @Getter
    @Setter
    private int power;

    @Getter
    private int hp;

    public Player(Position position, float halfSize, int hp, float speed) {
        super(halfSize, position, new PlayerMovement());
        this.hp = hp;
        this.speed = speed;
        power = 0;
        //We can be sure that the movement is a PlayerMovement because we have just defined it in the super constructor
        ((PlayerMovement) getMovement()).setOwner(this);
    }

    /**
     * Destroy the player automatically. Use when there is a direct hit with an enemy.
     */
    public void receiveMaxDamage() {
        receiveDamage(hp * 2);
    }

    /**
     * Give damage to the object. Drop a bit of power too. (Which may heart them psychologically more than hp !)
     * @param damage int
     */
    public void receiveDamage(int damage) {
        hp -= damage;
        setPower(Math.max(power - 2, 0));
    }

    @Override
    public void accept(GameObjectVisitor script) {
        script.visit(this);
    }
}