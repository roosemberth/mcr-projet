package model.gameObjects;

import model.Level;
import model.geometry.GeometryHelper;
import model.geometry.Position;
import model.managers.GameObjectVisitor;
import model.path.Path;

/**
 * Define an enemy that attacks and receives damage from the player. When destroyed it rewards the player with items.
 */
public class Enemy extends PathObject {
    private int hp;
    private boolean isAlive = true;

    public Enemy(Position position, float halfSize, Path path, int hp) {
        super(position, halfSize, path);
        this.hp = hp;
    }

    /**
     * Generate a random set of rewards for the player to collect. Currently generate only power point.
     * @param gameObjectList GameObjectList
     */
    private void randomRewards(GameObjectList gameObjectList) {
        for (int i = 0; i < (int) getHalfSize() / 10; ++i) {
            float angle = (float) Math.random() * 360f;
            float distance = (float) Math.random() * getHalfSize();
            Position spawn = new Position(getPosition().getX(), getPosition().getY()).add(GeometryHelper.getVectorFromAngleAndSpeed(angle, distance));
            Item item = new PowerItem(spawn, Level.ItemSize / 2);
            gameObjectList.add(item);
        }
    }

    /**
     * Get damaged when hit by a player bullet.
     * @param damage int
     * @param gameObjectList GameObjectList
     */
    public void receiveDamage(int damage, GameObjectList gameObjectList) {
        hp -= damage;
        if (isAlive && hp < 0) {
            gameObjectList.remove(this);
            Level.createExplosion(gameObjectList, 200, getPosition(), Level.EnemySize * 2);
            randomRewards(gameObjectList);
            isAlive = false; // Needed because multiple projectile can hit it in the same frame before it's dispose.
        }
    }

    @Override
    public void accept(GameObjectVisitor script) {
        script.visit(this);
    }
}