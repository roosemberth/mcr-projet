package model.gameObjects;

import lombok.Getter;
import model.commandMovement.CommandMovement;
import model.geometry.Position;

/**
 * Define an object receiving a movement from the command input.
 */
public abstract class CommandObject extends GameObject {
    @Getter
    private final CommandMovement movement;

    protected CommandObject(float halfSize, Position position, CommandMovement movement) {
        super(halfSize, position);
        this.movement = movement;
    }
}