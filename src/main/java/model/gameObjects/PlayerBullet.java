package model.gameObjects;

import model.Level;
import model.geometry.Position;
import model.managers.GameObjectVisitor;
import model.path.Path;

/**
 * Define a bullet created by the player or one of its allies.
 * It damages enemies.
 */
public class PlayerBullet extends Bullet {
    public PlayerBullet(Position position, Path path, int damage) {
        super(position, Level.PlayerBulletSize / 2, damage, path);
    }

    @Override
    public void accept(GameObjectVisitor script) {
        script.visit(this);
    }
}