package model;

/**
 * Interface to transmit information of the managers running time to the view.
 */
public interface DebugInformationReceiver {
    default void setManagerTime(String managerName, long time) {
    }
}