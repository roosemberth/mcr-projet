package model.script;

import model.Level;
import model.gameObjects.EnemyBullet;
import model.gameObjects.GameObject;
import model.gameObjects.GameObjectList;
import model.geometry.GeometryHelper;
import model.geometry.Vector;
import model.path.LinearPath;

/**
 * Create a shot in a strait line that is initially aimed towards a target object.
 */
public class AimedBombLancher extends CyclingScript {
    private final float bulletSpeed;
    private final float offsetAngle;

    public AimedBombLancher(int reloadTime, float bulletSpeed, float offsetAngle) {
        super(reloadTime);
        this.bulletSpeed = bulletSpeed;
        this.offsetAngle = offsetAngle;
    }

    @Override
    protected void executeAction(GameObject owner, GameObjectList other) {
        float angle = GeometryHelper.getAngleFromVector(new Vector(owner.getPosition(), other.player().getPosition()));
        angle += offsetAngle;

        other.add(
                new EnemyBullet(
                        owner.getPosition(),
                        Level.EnemyBulletSize / 2,
                        new LinearPath(GeometryHelper.getVectorFromAngleAndSpeed(angle, bulletSpeed)),
                        1));
    }
}