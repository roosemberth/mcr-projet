package model.script;

import model.Input;
import model.gameObjects.GameObject;
import model.gameObjects.GameObjectList;

/**
 * Abstract script for for a shot script that shot at periodic interval.
 */
public abstract class CyclingScript implements Script {
    private final int cycleInterval;
    private int lastCycle;

    protected CyclingScript(int cycleInterval) {
        this.cycleInterval = cycleInterval;
    }

    @Override
    public final void execute(GameObject owner, GameObjectList other, Input input, int currentTime) {
        if (currentTime > lastCycle + cycleInterval) {
            lastCycle = currentTime;
            executeAction(owner, other);
        }
    }

    protected abstract void executeAction(GameObject owner, GameObjectList other);
}