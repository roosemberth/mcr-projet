package model.script;

import model.Input;
import model.gameObjects.GameObject;
import model.gameObjects.GameObjectList;

/**
 * Destroy the object after some ticks cycle.
 * The object will be destroy while on screen after some time.
 */
public class DestroyedAtTimeOut implements Script {
    private int duration;

    public DestroyedAtTimeOut(int duration) {
        this.duration = duration;
    }

    @Override
    public void execute(GameObject owner, GameObjectList other, Input input, int currentTime) {
        if (duration-- <= 0) {
            other.remove(owner);
        }
    }
}