package model.script.shottype;

import model.Input;
import model.commandMovement.AllyShotTypeAPath;
import model.gameObjects.Ally;
import model.gameObjects.GameObjectList;
import model.gameObjects.Player;
import model.gameObjects.PlayerBullet;
import model.geometry.GeometryHelper;
import model.geometry.Vector;
import model.path.LinearPath;

/**
 * Define the shot behavior for the shot type A.
 * It shoots with an angle that change depending of the player movement mode (normal or focus).
 * The area of the shoot is wide even when focus.
 */
public class ShotTypeA extends ShotTypeReference {
    public ShotTypeA(Player player) {
        super(player);
    }

    @Override
    protected void createAllies(GameObjectList other, int powerLevel) {
        switch (powerLevel) { // Breaks are missing intentionally;
            case 3:
                add2SymetricAllies(40, 0, 20, 10, 2);
            case 2:
                add2SymetricAllies(10, -35, 10, 30, 6);
            case 1:
                add2SymetricAllies(30, 20, 3, 40, 8);
            case 0:
                add2SymetricAllies(30, -20, 15, 20, 4);
        }
    }

    @Override
    protected void shoot(GameObjectList other, Input inputs) {
        super.shoot(other, inputs);
        for (Ally a : getAllies()) {
            float angle = inputs.isFocus() ? a.getAngleShotFocus() : a.getAngleShot();
            other.add(new PlayerBullet(
                    a.getPosition(),
                    new LinearPath(GeometryHelper.getVectorFromAngleAndSpeed(angle, getBulletSpeed())),
                    1));
        }
    }

    private void add2SymetricAllies(int x, int y, float offsetAngle, int xFocus, float offsetAngleFocus) {
        float angle = 90;
        getAllies().add(new Ally(getPlayer(),
                angle - offsetAngle, angle - offsetAngleFocus,
                new AllyShotTypeAPath(getPlayer(),
                        new Vector(x, y),
                        new Vector(xFocus, 40), 1)));
        getAllies().add(new Ally(getPlayer(),
                angle + offsetAngle, angle + offsetAngleFocus,
                new AllyShotTypeAPath(getPlayer(), new Vector(-x, y),
                        new Vector(-xFocus, 40), 1)));
    }
}