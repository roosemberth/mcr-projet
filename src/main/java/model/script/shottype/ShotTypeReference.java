package model.script.shottype;

import lombok.AccessLevel;
import lombok.Getter;
import model.Input;
import model.gameObjects.*;
import model.geometry.GeometryHelper;
import model.geometry.Position;
import model.path.LinearPath;
import model.script.Script;

import java.util.LinkedList;
import java.util.List;

/**
 * Define how the player shoot. Also define how it's ally shoot.
 * Each types have there own levels.
 */
public abstract class ShotTypeReference implements Script {
    private final static int powerInterval = 10;
    private final static int maxPowerLevel = 3;
    private final static int shotInterval = 20;

    @Getter(AccessLevel.PROTECTED)
    private final int bulletSpeed = 5;

    @Getter(AccessLevel.PROTECTED)
    private final Player player;

    @Getter(AccessLevel.PROTECTED)
    private final List<Ally> allies = new LinkedList<>();

    private int powerLevel = -1;
    private int lastShoot = 0;

    public ShotTypeReference(Player player) {
        this.player = player;
    }

    @Override
    public void execute(GameObject owner, GameObjectList other, Input input, int currentTime) {
        checkLevel(other);
        if (input.isShot() && currentTime > lastShoot + shotInterval) {
            lastShoot = currentTime;
            shoot(other, input);
        }
    }

    private void checkLevel(GameObjectList other) {
        if (player.getPower() > powerInterval * maxPowerLevel) {
            player.setPower(powerInterval * maxPowerLevel);
        }

        int currentPowerLevel = player.getPower() / powerInterval;
        if (powerLevel == currentPowerLevel) {
            return;
        }

        powerLevel = currentPowerLevel;

        for (Ally a : allies) {
            other.remove(a);
        }

        allies.clear();

        createAllies(other, powerLevel);

        for (Ally a : allies) {
            other.add(a);
        }
    }

    protected abstract void createAllies(GameObjectList other, int powerLevel);

    protected void shoot(GameObjectList other, Input inputs) {
        Position p = player.getPosition();

        other.add(new PlayerBullet(
                new Position(p.getX() - 10, p.getY()),
                new LinearPath(GeometryHelper.getVectorFromAngleAndSpeed(90, bulletSpeed)),
                1));

        other.add(new PlayerBullet(
                new Position(p.getX() + 10, p.getY()),
                new LinearPath(GeometryHelper.getVectorFromAngleAndSpeed(90, bulletSpeed)),
                1));
    }
}
