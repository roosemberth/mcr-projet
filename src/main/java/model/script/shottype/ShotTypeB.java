package model.script.shottype;

import model.Input;
import model.commandMovement.AllyShotTypeBPath;
import model.gameObjects.Ally;
import model.gameObjects.GameObjectList;
import model.gameObjects.Player;
import model.gameObjects.PlayerBullet;
import model.geometry.GeometryHelper;
import model.path.LinearPath;

/**
 * Define the shot behavior for the shot type B.
 * It shoots with an angle that depends of the x axis distance with the player.
 * The area of the shot is relatively short even more in focus.
 */
public class ShotTypeB extends ShotTypeReference {
    public ShotTypeB(Player player) {
        super(player);
    }

    @Override
    protected void createAllies(GameObjectList other, int powerLevel) {
        switch (powerLevel) { // Breaks are missing intentionally;
            case 3:
                addAlly(0, 50);
                addAlly(72, 50);
                addAlly(144, 50);
                addAlly(216, 50);
                addAlly(288, 50);
                break;
            case 2:
                addAlly(0, 40);
                addAlly(120, 40);
                addAlly(240, 40);
                break;
            case 1:
                addAlly(180, 40);
            case 0:
                addAlly(0, 40);
        }
    }

    @Override
    protected void shoot(GameObjectList other, Input inputs) {
        super.shoot(other, inputs);
        for (Ally a : getAllies()) {
            float angle = a.getAngleShot();
            float disX = getPlayer().getPosition().getX() - a.getPosition().getX();
            angle += disX / 5; // shot offset
            other.add(new PlayerBullet(
                    a.getPosition(),
                    new LinearPath(GeometryHelper.getVectorFromAngleAndSpeed(angle, getBulletSpeed())),
                    1));
        }
    }

    private void addAlly(float anglePosition, float distance) {
        float angleShot = 90;
        getAllies().add(new Ally(getPlayer(),
                angleShot, angleShot,
                new AllyShotTypeBPath(getPlayer(), anglePosition, distance)));
    }
}