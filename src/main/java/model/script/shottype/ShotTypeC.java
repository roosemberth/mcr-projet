package model.script.shottype;

import model.Input;
import model.commandMovement.AllyShotTypeCPath;
import model.gameObjects.*;
import model.geometry.GeometryHelper;
import model.path.LinearPath;

/**
 * Define the shot behavior for the shot type C.
 * It shoots in straight line forwards.
 * The area of the shot depends of the position of the allies that follow the player in a snake pattern.
 */
public class ShotTypeC extends ShotTypeReference {
    public ShotTypeC(Player player) {
        super(player);
    }

    @Override
    protected void createAllies(GameObjectList other, int powerLevel) {
        addAlly(getPlayer(), 50);
        switch (powerLevel) { // Breaks are missing intentionally;
            case 3:
                addAlly(getAllies().get(getAllies().size() - 1), 30);
            case 2:
                addAlly(getAllies().get(getAllies().size() - 1), 30);
            case 1:
                addAlly(getAllies().get(getAllies().size() - 1), 30);
            case 0:
        }
    }

    @Override
    protected void shoot(GameObjectList other, Input inputs) {
        super.shoot(other, inputs);
        float angle = 90;
        for (Ally a : getAllies()) {
            other.add(new PlayerBullet(
                    a.getPosition(),
                    new LinearPath(GeometryHelper.getVectorFromAngleAndSpeed(angle, getBulletSpeed())),
                    1));
        }
    }

    private void addAlly(GameObject tail, float distance) {
        float angleShot = 90;
        getAllies().add(new Ally(getPlayer(),
                angleShot, angleShot,
                new AllyShotTypeCPath(tail, distance)));
    }
}