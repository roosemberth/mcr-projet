package model.script;

import model.Input;
import model.gameObjects.GameObject;
import model.gameObjects.GameObjectList;

/**
 * Define a behavior that will be apply to an object. The script can create a new object, react to an user input or
 * some behavior depending of the current time. Each script contains it own logic that will affect the game field.
 */
@FunctionalInterface
public interface Script {
    void execute(GameObject owner, GameObjectList other, Input inputs, int currentTime);
}