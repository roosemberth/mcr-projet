package model.script;

import model.Level;
import model.gameObjects.EnemyBullet;
import model.gameObjects.GameObject;
import model.gameObjects.GameObjectList;
import model.geometry.GeometryHelper;
import model.path.LinearPath;

/**
 * Create a shot in a straight line at regular interval.
 */
public class BombLauncher extends CyclingScript {
    private final int bulletDamage;
    private final LinearPath path;

    public BombLauncher(int reloadTime, float bulletSpeed, int bulletDamage, float angle) {
        super(reloadTime);
        this.bulletDamage = bulletDamage;
        path = new LinearPath(GeometryHelper.getVectorFromAngleAndSpeed(angle, bulletSpeed));
    }

    @Override
    protected void executeAction(GameObject owner, GameObjectList other) {
        other.add(new EnemyBullet(owner.getPosition(), Level.EnemyBulletSize / 2, path, bulletDamage));
    }
}