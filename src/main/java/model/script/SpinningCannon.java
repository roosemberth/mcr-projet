package model.script;

import model.Level;
import model.gameObjects.EnemyBullet;
import model.gameObjects.GameObject;
import model.gameObjects.GameObjectList;
import model.geometry.GeometryHelper;
import model.path.LinearPath;

/**
 * Creates a set of projectiles going in a spinning pattern. The speed of the spin keep accelerating over time.
 * There can be multiple tails to the spin pattern.
 */
public class SpinningCannon extends CyclingScript {
    private final int angleOffset;
    private final int sourceCount;

    private final float spiralAccel;

    private final float bulletSpeed;
    private final int bulletDamage;

    private float angle = 90;
    private float offset;

    public SpinningCannon(int reloadTime, int sourceCount, float spiralAccel, float bulletSpeed, int bulletDamage) {
        super(reloadTime);
        this.sourceCount = sourceCount;
        this.bulletSpeed = bulletSpeed;
        this.bulletDamage = bulletDamage;
        this.spiralAccel = spiralAccel;
        angleOffset = 360 / sourceCount;
    }

    @Override
    protected void executeAction(GameObject owner, GameObjectList other) {
        for (int i = 0; i < sourceCount; i++) {
            other.add(
                    new EnemyBullet(
                            owner.getPosition(),
                            Level.EnemyBulletSize / 2,
                            new LinearPath(GeometryHelper.getVectorFromAngleAndSpeed(angle + angleOffset * i, bulletSpeed)),
                            bulletDamage));
        }

        angle -= offset;
        angle = angle > 0 ? angle + 360 : angle;
        offset += spiralAccel;
    }
}