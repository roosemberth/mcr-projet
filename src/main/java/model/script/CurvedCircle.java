package model.script;

import model.Level;
import model.gameObjects.EnemyBullet;
import model.gameObjects.GameObject;
import model.gameObjects.GameObjectList;
import model.path.CurvedPath;

/**
 * Creates some circles set of bullets moving in a curved formation.
 */
public class CurvedCircle extends CyclingScript {
    private final int density;
    private final float bulletSpeed;
    private final float offset;
    private final float angle;
    private final float spinSpeed;
    private float initAngle;

    public CurvedCircle(int cycleInterval, int density, float bulletSpeed, float offset, float spinSpeed) {
        super(cycleInterval);
        this.density = density;
        this.bulletSpeed = bulletSpeed;
        this.offset = offset;
        this.spinSpeed = spinSpeed;
        angle = 360f / density;
        initAngle = 0;
    }

    @Override
    protected void executeAction(GameObject owner, GameObjectList other) {
        initAngle += spinSpeed;
        for (int i = 0; i < density; ++i) {
            other.add(new EnemyBullet(
                    owner.getPosition(),
                    Level.EnemyBulletSize / 2,
                    new CurvedPath((angle * i - initAngle) + 180, bulletSpeed, offset),
                    1));
        }
    }
}
