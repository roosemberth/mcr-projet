package model.levelDescription;

import lombok.Getter;
import model.Level;
import model.gameObjects.Enemy;
import model.gameObjects.GameObjectList;
import model.geometry.Position;
import model.path.Path;

/**
 * Define a section. A section will generate a bunch of enemies and end when there are none left.
 * Some delay will prevent the section from immediately ends. This is because it can be confusing to be shooting
 * at the final boss and having the result screen abruptly display without seeing the boss explode.
 * It also give some breathing room between sections.
 */
public abstract class Section {
    @Getter
    protected boolean sectionEnd = false;
    protected int enemyCount;
    protected int delay = 500; // to delay the next section.

    /**
     * Check if the section has ended completely. (Delay included)
     * @param gameObjectList GameObject
     * @param currentTime int
     */
    protected void checkSectionProgress(GameObjectList gameObjectList, int currentTime) {
        sectionEnd = (enemyCount <= 0 && !gameObjectList.enemies().iterator().hasNext() && delay-- <= 0);
    }

    /**
     * Update the current section. More detail depending within individual section.
     * @param gameObjectList GameObjectList
     * @param currentTime int
     */
    public void update(GameObjectList gameObjectList, int currentTime) {
        checkSectionProgress(gameObjectList, currentTime);
    }

    protected Enemy createEnemy(Position startPos, Path path, int hp) {
        return new Enemy(startPos, Level.EnemySize / 2, path, hp);
    }
}
