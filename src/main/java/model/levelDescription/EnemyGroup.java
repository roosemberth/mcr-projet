package model.levelDescription;

import lombok.Data;
import model.gameObjects.Enemy;

import java.util.List;

@Data
public class EnemyGroup {
    private final Iterable<Enemy> enemies;
    private int spawnTime;

    public EnemyGroup(int spawnTime, Enemy... enemies) {
        this.spawnTime = spawnTime;
        this.enemies = List.of(enemies);
    }
}