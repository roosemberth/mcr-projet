package model.levelDescription.firstLevel;

import model.Level;
import model.gameObjects.Enemy;
import model.gameObjects.GameObjectList;
import model.geometry.Position;
import model.levelDescription.Section;
import model.options.Difficulty;
import model.path.StationaryPath;
import model.script.SpinningCannon;

public class BossSection extends Section {
    private int canonSource;

    public BossSection(Difficulty difficulty) {
        enemyCount = 1;
        delay = 1000;
        switch (difficulty) {
            case EASY:
                canonSource = 1;
                break;
            case NORMAL:
                canonSource = 2;
                break;
            case HARD:
                canonSource = 5;
                break;
        }
    }

    @Override
    public void update(GameObjectList gameObjectList, int currentTime) {
        super.update(gameObjectList, currentTime);

        if (enemyCount-- > 0) {
            gameObjectList.add(createBoss(canonSource));
        }
    }

    private Enemy createBoss(int cannonSourceCount) {
        Enemy enemy = new Enemy(
                new Position(500F, 700F),
                Level.EnemySize / 2,
                new StationaryPath(),
                1000);
        enemy.addScripts(new SpinningCannon(5, cannonSourceCount, 0.1F, 1.5F, 1));
        return enemy;
    }
}