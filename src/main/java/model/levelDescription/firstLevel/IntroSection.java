package model.levelDescription.firstLevel;

import model.Level;
import model.gameObjects.Enemy;
import model.gameObjects.GameObjectList;
import model.geometry.Position;
import model.levelDescription.Section;
import model.options.Difficulty;
import model.path.RoundTripPath;
import model.script.BombLauncher;

public class IntroSection extends Section {

    private final float ratioSpawnInterval;
    private int intervalSpawn;
    private int intervalShot;
    private float bulletSpeed;

    public IntroSection(Difficulty difficulty) {
        bulletSpeed = 1f;
        intervalSpawn = 1000;
        ratioSpawnInterval = 0.97f;
        switch (difficulty) {
            case EASY:
                intervalShot = 500;
                enemyCount = 8;
                break;
            case NORMAL:
                intervalShot = 100;
                bulletSpeed = 1.5f;
                enemyCount = 16;
                break;
            case HARD:
                intervalShot = 80;
                bulletSpeed = 2f;
                enemyCount = 24;
                break;
        }
    }

    @Override
    public void update(GameObjectList gameObjectList, int currentTime) {
        super.update(gameObjectList, currentTime);

        if (enemyCount > 0 && currentTime % intervalSpawn == 0) {
            enemyCount--;
            intervalSpawn *= ratioSpawnInterval;
            gameObjectList.add(
                    createStandardEnemy(
                            new Position((float) Math.random() * 960f + 20f, (float) Math.random() * 200f + 700f)));
        }
    }

    private Enemy createStandardEnemy(Position startPos) {
        Enemy enemy = new Enemy(startPos,
                Level.EnemySize / 2,
                new RoundTripPath(new Position(20F, startPos.getY()), new Position(980F, startPos.getY()), 1f),
                50);
        enemy.addScripts(new BombLauncher(intervalShot, bulletSpeed, 1, 270));
        return enemy;
    }
}
