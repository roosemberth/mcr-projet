package model.levelDescription.firstLevel;

import model.levelDescription.LevelDescription;
import model.options.Difficulty;

public class FirstLevel extends LevelDescription {

    public FirstLevel(Difficulty difficulty) {
        getSections().add(new IntroSection(difficulty));
        getSections().add(new BossSection(difficulty));
        initialize();
    }
}