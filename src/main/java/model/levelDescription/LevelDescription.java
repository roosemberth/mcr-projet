package model.levelDescription;

import lombok.AccessLevel;
import lombok.Getter;
import model.Level;
import model.gameObjects.*;
import model.geometry.GeometryHelper;
import model.geometry.Position;
import model.options.ShotType;
import model.path.LinearPath;
import model.script.DestroyedAtTimeOut;
import model.script.shottype.ShotTypeA;
import model.script.shottype.ShotTypeB;
import model.script.shottype.ShotTypeC;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Define what a level must contains. A level is divided in sections.
 * Section create enemies and end when they all vanquished.
 * When a section end we go to the next one untile all section have been completed.
 */
public abstract class LevelDescription {
    @Getter(AccessLevel.PROTECTED)
    private final List<Section> sections = new LinkedList<>();

    private Iterator<Section> s;
    private Section currentSection;

    @Getter
    private boolean levelEnd = false;

    /**
     * Initialize the first section.
     */
    public void initialize() {
        s = sections.iterator();
        if (s.hasNext()) {
            currentSection = s.next();
        } else {
            throw new NoSuchElementException("Missing section");
        }
    }

    /**
     * Create a player and its allies based on the shot type.
     * @param type ShotType
     * @return Player
     */
    public Player createPlayer(ShotType type) {
        Player p = new Player(
                new Position(500f, 100f),
                Level.PlayerSize / 2, 10, 2.5F);

        switch (type) {
            case TypeA:
                p.addScripts(new ShotTypeA(p));
                break;
            case TypeB:
                p.addScripts(new ShotTypeB(p));
                break;
            case TypeC:
                p.addScripts(new ShotTypeC(p));
                break;
        }

        return p;
    }

    /**
     * Update the current section until it ends. Then it goes to the next one.
     * If all section are finished then the level is itself finished.
     * More is define in specific levels extending this class.
     * @param gameObjectList GameObjectList
     * @param currentTime int
     */
    public void update(GameObjectList gameObjectList, int currentTime) {
        if (currentSection.isSectionEnd()) {
            if (s.hasNext()) {
                currentSection = s.next();
            } else {
                levelEnd = true;
            }
            return;
        }

        currentSection.update(gameObjectList, currentTime);
    }
}