package model.levelDescription.SecondLevel;

import model.levelDescription.LevelDescription;
import model.options.Difficulty;

public class SecondLevel extends  LevelDescription {

    public SecondLevel(Difficulty difficulty) {
        getSections().add(new IntroSection(difficulty));
        getSections().add(new BossSection(difficulty));

        initialize();
    }
}
