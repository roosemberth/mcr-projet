package model.levelDescription.SecondLevel;

import model.gameObjects.Enemy;
import model.gameObjects.GameObjectList;
import model.geometry.Position;
import model.levelDescription.Section;
import model.options.Difficulty;
import model.path.RandomPathInBound;
import model.script.AimedBombLancher;

public class IntroSection extends Section {
    int intervalSpawn;
    int shotInterval;
    int hpEnemy;
    int density;
    float bulletSpeed;

    public IntroSection(Difficulty difficulty) {
        switch (difficulty) {
            case EASY:
                intervalSpawn = 500;
                shotInterval = 500;
                hpEnemy = 30;
                density = 0;
                enemyCount = 20;
                bulletSpeed = 1f;
                break;
            case NORMAL:
                intervalSpawn = 500;
                shotInterval = 500;
                hpEnemy = 30;
                density = 1;
                enemyCount = 20;
                bulletSpeed = 1.5f;
                break;
            case HARD:
                intervalSpawn = 400;
                shotInterval = 200;
                hpEnemy = 50;
                density = 5;
                enemyCount = 30;
                bulletSpeed = 2f;
                break;
        }
    }

    @Override
    public void update(GameObjectList gameObjectList, int currentTime) {
        super.update(gameObjectList,currentTime);

        if (enemyCount > 0 && currentTime % intervalSpawn == 0) {
            enemyCount--;

            if (enemyCount == 10) {
                intervalSpawn = 1;
            }
            Position spawnPos = new Position(500f,1000);
            Enemy e = createEnemy(
                    spawnPos,
                    new RandomPathInBound(spawnPos,1,30,500, 970, 450),
                    hpEnemy);

            e.addScripts(new AimedBombLancher(shotInterval, bulletSpeed, 0));
            float offset = 20;
            for(int i = 1; i <= density; ++i) {
                e.addScripts(new AimedBombLancher(shotInterval, bulletSpeed, offset*i));
                e.addScripts(new AimedBombLancher(shotInterval, bulletSpeed, -offset*i));
            }
            gameObjectList.add(e);
        }
    }
}
