package model.levelDescription.SecondLevel;

import model.gameObjects.Enemy;
import model.gameObjects.GameObjectList;
import model.geometry.Position;
import model.levelDescription.Section;
import model.options.Difficulty;
import model.path.StationaryPath;
import model.script.CurvedCircle;

public class BossSection extends Section {
    int shotInterval;
    int hpEnemy;
    int density;
    float bulletSpeed;
    float offset;
    float spinSpeed;

    public BossSection(Difficulty difficulty) {
        enemyCount = 1;
        hpEnemy = 1000;
        delay = 1000;
        bulletSpeed = 1.5f;

        switch (difficulty) {
            case EASY:
                shotInterval = 50;
                density = 8;
                offset = 0;
                spinSpeed = 8;
                break;
            case NORMAL:
                shotInterval = 50;
                density = 8;
                offset = 0.1f;
                spinSpeed = 6;
                break;
            case HARD:
                shotInterval = 40;
                density = 12;
                offset = 0.1f;
                spinSpeed = 4;
                break;
        }
    }

    @Override
    public void update(GameObjectList gameObjectList, int currentTime) {
        super.update(gameObjectList,currentTime);

        if (enemyCount > 0) {
            enemyCount--;

            Enemy b = createEnemy(new Position(500f,800), new StationaryPath(), hpEnemy);

            b.addScripts(new CurvedCircle(shotInterval,density,bulletSpeed,offset, spinSpeed));
            b.addScripts(new CurvedCircle(shotInterval,density,bulletSpeed,-offset, -spinSpeed));

            gameObjectList.add(b);
        }
    }
}
