package model;

import lombok.Getter;

import java.util.Set;

/**
 * User inputs. The user can move in the 4 cardinals direction. There is a shot command and a focus command.
 * The focus command slows down the player movement for more precise navigation in the game field.
 */
public class Input {

    @Getter
    private final boolean shot, focus;

    @Getter
    private final float x;

    @Getter
    private final float y;

    public Input(Set<Commands> inputsActive) {
        shot = inputsActive.contains(Commands.SHOT);
        focus = inputsActive.contains(Commands.FOCUS);
        float y = (inputsActive.contains(Commands.UP) ? 1 : 0) + (inputsActive.contains(Commands.DOWN) ? -1 : 0);
        float x = (inputsActive.contains(Commands.RIGHT) ? 1 : 0) + (inputsActive.contains(Commands.LEFT) ? -1 : 0);

        // Diagonal movement
        if (x != 0) {
            y *= 0.707;
        }

        // Diagonal movement
        if (y != 0) {
            x *= 0.707;
        }

        // Slows down by 2 the speed movement.
        if (focus) {
            x *= 0.5;
            y *= 0.5;
        }

        this.x = x;
        this.y = y;
    }

    public enum Commands {UP, DOWN, LEFT, RIGHT, FOCUS, SHOT}
}