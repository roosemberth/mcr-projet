package model;

import model.gameObjects.Decoration;
import model.gameObjects.GameObject;
import model.gameObjects.GameObjectList;
import model.geometry.GeometryHelper;
import model.geometry.Position;
import model.levelDescription.LevelDescription;
import model.levelDescription.SecondLevel.SecondLevel;
import model.levelDescription.firstLevel.FirstLevel;
import model.managers.CollisionEngine;
import model.managers.FixedManager;
import model.managers.Manager;
import model.managers.MovementEngine;
import model.options.Options;
import model.path.LinearPath;
import model.script.DestroyedAtTimeOut;

import java.util.*;

/**
 * Level manager to simulate the next tick and run the engines/managers to apply to the game.
 */
public class Level {
    public static final float PlayerSize = 10F, EnemySize = 60F, PlayerBulletSize = 15F, EnemyBulletSize = 20F,
            ItemSize = 20F, ExplosionSize = 7F, AllySize = 15F;

    public static final float SIMULATION_WIDTH = 1000F, SIMULATION_HEIGHT = 1000F;

    private final Collection<Manager> dynamicUpdateManagers = new LinkedList<>();
    private final Collection<FixedManager> fixedUpdateManagers = new LinkedList<>();

    private final GameObjectList gameObjects;
    private final LevelDescription description;

    private final long tickLength;
    private long timeShift;
    private int currentTickNumber;

    /**
     * Constructor. Initialise the game based on the options modifiers.
     *
     * @param options Options
     */
    public Level(Options options) {
        tickLength = (long) ((1000000000 / 240) / options.getGameSpeed());
        this.description = selectLevel(options);
        gameObjects = new GameObjectList(description.createPlayer(options.getShotType()));
        addFixedManager(new CollisionEngine(gameObjects));
        addFixedManager(new MovementEngine(gameObjects));
    }

    /**
     * Utility to create an explosion effect.
     *
     * @param gameObjectList GameObjectList
     * @param nb             int
     * @param p              Position
     * @param size           float
     */
    public static void createExplosion(GameObjectList gameObjectList, int nb, Position p, float size) {
        for (int i = 0; i < nb; ++i) {
            float angle = (float) Math.random() * 360f;
            float speed = ((float) Math.random() * .005f + .005f) * size;
            int duration = (int) (Math.random() * 100 + 50);
            Decoration explosion =
                    new Decoration(
                            new Position(p.getX(), p.getY()),
                            Level.ExplosionSize / 2,
                            new LinearPath(GeometryHelper.getVectorFromAngleAndSpeed(angle, speed), 0.99f));
            explosion.addScripts(new DestroyedAtTimeOut(duration));
            gameObjectList.add(explosion);
        }
    }

    /**
     * Load the correct level depending of the level selected in the option.
     *
     * @param options Options
     * @return LevelDescription
     */
    public static LevelDescription selectLevel(Options options) {
        switch (options.getLevelSelected()) {
            case FirstLevel:
                return new FirstLevel(options.getDifficulty());
            case SecondLevel:
                return new SecondLevel(options.getDifficulty());
            default:
                throw new NoSuchElementException();
        }
    }

    /**
     * Add a dynamic manager. Right now this is only the RenderEngine.
     * It does not run along with the other managers. It runs instead every frame.
     *
     * @param manager Manager
     */
    public void addDynamicManager(Manager manager) {
        dynamicUpdateManagers.add(manager);
    }

    /**
     * Add a fixed manager. This is currently the collision manager and the movement manager.
     * They run every ticks.
     *
     * @param manager FixedManager
     */
    public void addFixedManager(FixedManager manager) {
        fixedUpdateManagers.add(manager);
    }

    /**
     * Run a simulation of the game for one unit of time. (a tick)
     * During a tick, every manager apply actions to the game object.
     * Next object possessing scripts runs there specialised actions.
     *
     * @param inputs       Input
     * @param managersTime Map
     */
    private void tick(Input inputs, Map<String, Long> managersTime) {
        currentTickNumber++;

        // Run managers
        for (FixedManager manager : fixedUpdateManagers) {
            long start = System.nanoTime();
            for (GameObject object : gameObjects) {
                object.accept(manager);
            }
            managersTime.merge(manager.getName(), (System.nanoTime() - start) / 1000, Long::sum);
        }

        // Run scripts
        for (GameObject gameObject : gameObjects) {
            gameObject.executeScripts(gameObjects, inputs, currentTickNumber);
        }

        // update information for the view
        description.update(gameObjects, currentTickNumber);

        // update the list of game object. (Add and remove based on what happened during this tick.)
        gameObjects.update();
    }

    /**
     * Run a simulation of the game for every unit of time occurring between frames.
     * This function is run every frame. This meant that the game will be simulated a number of time util reaching
     * the current time of the frame required for the frame.
     *
     * @param inputs                   Input
     * @param tickNumber               int
     * @param debugInformationReceiver DebugInformationReceiver
     */
    private void tick(Input inputs, int tickNumber, DebugInformationReceiver debugInformationReceiver) {
        Map<String, Long> managersTime = new HashMap<>();

        if (tickNumber > 0) {
            for (FixedManager manager : fixedUpdateManagers) {
                manager.setCurrentFrameInfos(inputs);
                managersTime.put(manager.getName(), 0L);
            }

            for (int i = 0; i < tickNumber; i++) {
                tick(inputs, managersTime);
            }
        }

        managersTime.forEach(debugInformationReceiver::setManagerTime);
    }

    /**
     * Update the game logic. This require to calculate how many time has passed since the last frame and update the
     * game up to the current frame. This function also runs the dynamic manager that have to be run once every frame.
     * We also check the game status to detect if the game has ended.
     *
     * @param inputs                   Input
     * @param timeSinceLastFrame       long
     * @param debugInformationReceiver DebugInformationReceiver
     * @return GameState
     */
    public GameState update(Input inputs, long timeSinceLastFrame, DebugInformationReceiver debugInformationReceiver) {
        timeShift += timeSinceLastFrame;

        long ticksNumber = timeShift / tickLength;
        timeShift -= tickLength * ticksNumber;

        tick(inputs, (int) ticksNumber, debugInformationReceiver);

        for (Manager manager : dynamicUpdateManagers) {
            long start = System.nanoTime();
            for (GameObject object : gameObjects) {
                object.accept(manager);
            }
            debugInformationReceiver.setManagerTime(manager.getName(), (System.nanoTime() - start) / 1000);
        }

        if (gameObjects.player().getHp() < 0) {
            return GameState.lost;
        }

        if (description.isLevelEnd()) {
            return GameState.won;
        }
        return GameState.inProgress;
    }

    public int getBulletCount() {
        return gameObjects.bulletCount();
    }

    /**
     * List of game state.
     */
    public enum GameState {inProgress, lost, won}
}