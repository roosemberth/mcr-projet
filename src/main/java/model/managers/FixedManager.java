package model.managers;

import model.Input;

/**
 * Define a manager that is run every ticks.
 * It's to differentiate with the RenderEngine which is executed every frame instead.
 */
public interface FixedManager extends Manager {
    void setCurrentFrameInfos(Input input);
}