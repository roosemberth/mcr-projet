package model.managers;

import model.gameObjects.*;

/**
 * Define an object (manager/engine) that will apply an different actions depending on the object.
 * There are currently 5 objects implementing this class.
 * RenderEngine, CollisionEngine, MovementEngine and 2 within GameObjectList for its own list management.
 */
public interface GameObjectVisitor {
    void visit(Player o);

    void visit(Enemy o);

    void visit(EnemyBullet o);

    void visit(PlayerBullet o);

    void visit(Item item);

    void visit(Decoration decoration);

    void visit(Ally ally);
}