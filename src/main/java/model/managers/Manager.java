package model.managers;

/**
 * Define a manager that will manipulate game objects. Each manager needs a name for the debug display.
 */
public interface Manager extends GameObjectVisitor {
    String getName();
}