package model.managers;

import model.Input;
import model.Level;
import model.gameObjects.*;
import model.geometry.Position;

/**
 * Define the action to take when a collision occurs.
 * This class implements FixedManager which implements GameObjectVisitor in order to treat each game object differently.
 */
public class CollisionEngine implements FixedManager {
    private final GameObjectList gameObjectList;

    public CollisionEngine(GameObjectList gameObjectList) {
        this.gameObjectList = gameObjectList;
    }

    /**
     * Test whether two objects overlap.
     */
    private static boolean checkCollision(GameObject o1, GameObject o2) {
        return checkCollision(o1.getPosition(), o1.getHalfSize(), o2.getPosition(), o2.getHalfSize());
    }

    /**
     * Test whether two objects overlap. All objects are some sort of circle.
     * So the collision is define has if the cumulative size between the half size of each object is bigger than
     * the distance between them, then there is collision.
     * We simplified the equation to avoid a square root calculation.
     */
    private static boolean checkCollision(Position p1, float halfSize1, Position p2, float halfSize2) {
        float radiusSquare = halfSize1 + halfSize2;
        radiusSquare *= radiusSquare;

        float disXSquare = Math.abs(p1.getX() - p2.getX());
        disXSquare *= disXSquare;

        float disYSquare = Math.abs(p1.getY() - p2.getY());
        disYSquare *= disYSquare;

        return radiusSquare > disXSquare + disYSquare;
    }

    @Override
    public void visit(Player o) {
        for (Item i : gameObjectList.items()) {
            if (checkCollision(o.getPosition(), o.getHalfSize() * 20, i.getPosition(), i.getHalfSize())) {
                i.attractByPlayer(o);
            }
        }
    }

    @Override
    public void visit(Enemy o) {
        if (checkCollision(o, gameObjectList.player())) {
            gameObjectList.player().receiveMaxDamage();
        }
    }

    @Override
    public void visit(EnemyBullet o) {
        if (checkCollision(o, gameObjectList.player())) {
            gameObjectList.player().receiveDamage(o.getDamage());
            gameObjectList.remove(o);
            Level.createExplosion(gameObjectList, 50, o.getPosition(), Level.PlayerSize * 10);
        }
    }

    @Override
    public void visit(PlayerBullet o) {
        for (Enemy enemy : gameObjectList.enemies()) {
            if (checkCollision(o, enemy)) {
                enemy.receiveDamage(o.getDamage(), gameObjectList);
                gameObjectList.remove(o);
                Level.createExplosion(gameObjectList, 1, o.getPosition(), 400);
                return;
            }
        }
    }

    @Override
    public void visit(Item o) {
        Player p = gameObjectList.player();
        if (checkCollision(o, p)) {
            o.reward(p);
            gameObjectList.remove(o);
        }
    }

    @Override
    public void visit(Decoration o) {
        // no collision
    }

    @Override
    public void visit(Ally o) {
        // Can collect item for the player.
        for (Item i : gameObjectList.items()) {
            if (checkCollision(o, i)) {
                i.reward(o.getPlayer());
                gameObjectList.remove(i);
            }
        }
    }

    @Override
    public void setCurrentFrameInfos(Input input) {
    }

    @Override
    public String getName() {
        return "Collision Engine";
    }
}