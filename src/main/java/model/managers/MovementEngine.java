package model.managers;

import model.Input;
import model.Level;
import model.gameObjects.*;
import model.geometry.Position;

/**
 * Defines how objects will move. Implements FixedManager that implements GameObjectList.
 */
public class MovementEngine implements FixedManager {
    private final GameObjectList gameObjectList;
    private Input currentInput;

    public MovementEngine(GameObjectList gameObjectList) {
        this.gameObjectList = gameObjectList;
    }

    /**
     * Prevent an object from getting out of the screen. It's used for the player.
     * More precisely it return from a coordinate a new one in bound if the previous one was not.
     * @param p Position
     * @return Position
     */
    private static Position stayInBound(Position p) {
        final int offset = 50;
        if (p.getX() - offset < 0) {
            p = p.withX(offset);
        } else if (p.getX() + offset > Level.SIMULATION_WIDTH) {
            p = p.withX(Level.SIMULATION_WIDTH - offset);
        }

        if (p.getY() - offset < 0) {
            p = p.withY(offset);
        } else if (p.getY() + offset > Level.SIMULATION_HEIGHT) {
            p = p.withY(Level.SIMULATION_HEIGHT - offset);
        }

        return p;
    }

    /**
     * Check if the object is completely off screen with some margin distance. Used for example when we want to delete
     * an object that left the game field.
     * @param o GameObject
     * @return boolean
     */
    private static boolean checkFullyOffScreen(GameObject o) {
        Position p = o.getPosition();

        //This margin is used so that objects with a display size larger than their collision box are not deleted while
        // part of their display is still in the screen
        final float margin = 100;

        return p.getX() + o.getHalfSize() + margin < 0 ||
                p.getY() + o.getHalfSize() + margin < 0 ||
                p.getX() - o.getHalfSize() - margin > Level.SIMULATION_WIDTH ||
                p.getY() - o.getHalfSize() - margin > Level.SIMULATION_HEIGHT;
    }

    /**
     * Delete the object if it has gone offscreen.
     * @param o GameObject
     */
    private void deleteOffScreen(GameObject o) {
        if (checkFullyOffScreen(o)) {
            gameObjectList.remove(o);
        }
    }

    /**
     * Move an object and deletes it if, following the movement, it has left the screen.
     * @param po PathObject
     */
    private void moveAndDeleteOffScreen(PathObject po) {
        po.setPosition(po.getPath().move(po.getPosition()));
        deleteOffScreen(po);
    }

    @Override
    public void visit(Player o) {
        o.setPosition(stayInBound(o.getMovement().move(o.getPosition(), currentInput)));
    }

    @Override
    public void visit(Enemy o) {
        moveAndDeleteOffScreen(o);
    }

    @Override
    public void visit(EnemyBullet o) {
        moveAndDeleteOffScreen(o);
    }

    @Override
    public void visit(PlayerBullet o) {
        moveAndDeleteOffScreen(o);
    }

    @Override
    public void visit(Item o) {
        moveAndDeleteOffScreen(o);
    }

    @Override
    public void visit(Decoration o) {
        moveAndDeleteOffScreen(o);
    }

    @Override
    public void visit(Ally o) {
        o.setPosition(o.getMovement().move(o.getPosition(), currentInput));
    }

    @Override
    public void setCurrentFrameInfos(Input input) {
        currentInput = input;
    }

    @Override
    public String getName() {
        return "Movement engine";
    }
}