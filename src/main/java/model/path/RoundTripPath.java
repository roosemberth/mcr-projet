package model.path;

import model.geometry.Position;
import model.geometry.Vector;

/**
 * Define a path going back and forth between two point.
 */
public class RoundTripPath extends Path {
    private final Position first, second;
    private final float speed;
    private boolean goToSecond;

    public RoundTripPath(Position first, Position second, float speed) {
        this.first = first;
        this.second = second;
        this.speed = speed;
    }

    @Override
    public Position move(Position p) {
        float distance = speed;
        while (true) {
            Vector target;
            if (goToSecond) {
                target = new Vector(p, second);
            } else {
                target = new Vector(p, first);
            }

            float lenght = target.lenght();

            if (lenght > distance) {
                return p.add(target.multiply(distance / lenght));
            } else {
                p = p.add(target);
                goToSecond = !goToSecond;
                distance -= lenght;
            }
        }
    }
}