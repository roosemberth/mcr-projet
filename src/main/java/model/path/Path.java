package model.path;

import model.geometry.Position;

/**
 * Define a path to follow. Concretely, define from a position what the next position should be.
 */
public abstract class Path {
    public abstract Position move(Position p);
}