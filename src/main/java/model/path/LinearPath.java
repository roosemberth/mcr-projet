package model.path;

import model.geometry.Position;
import model.geometry.Vector;

/**
 * Define a linear path. Follow a straight line and can accelerate or decelerate while going.
 */
public class LinearPath extends Path {
    private final float accel;
    private Vector direction;

    public LinearPath(Vector v) {
        this(v, 1f);
    }

    public LinearPath(float x, float y) {
        this(new Vector(x, y), 1f);
    }

    public LinearPath(Vector v, float acceleration) {
        direction = v;
        accel = acceleration;
    }

    public LinearPath(float x, float y, float acceleration) {
        this(new Vector(x, y), acceleration);
    }

    @Override
    public Position move(Position p) {
        direction = direction.multiply(accel);
        return p.add(direction);
    }
}