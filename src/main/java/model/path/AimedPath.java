package model.path;

import model.gameObjects.GameObject;
import model.geometry.Position;
import model.geometry.Vector;

/**
 * Define a path aimed at a target object. Makes it follow around, despite the object moving away.
 */
public class AimedPath extends Path {
    private final GameObject target;
    private final float speed;

    public AimedPath(GameObject target, float speed) {
        this.target = target;
        this.speed = speed;
    }

    @Override
    public Position move(Position p) {
        return p.add((new Vector(p, target.getPosition())).resized(speed));
    }
}