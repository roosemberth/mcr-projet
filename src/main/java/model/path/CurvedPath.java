package model.path;

import model.geometry.GeometryHelper;
import model.geometry.Position;

/**
 * Define a curved path. The angle to go next keep changing after every movement by an offset.
 */
public class CurvedPath extends Path {
    private final float speed;
    private final float offset;
    private float angle;

    public CurvedPath(float initAngle, float speed, float offset) {
        this.angle = initAngle;
        this.speed = speed;
        this.offset = offset;
    }

    @Override
    public Position move(Position p) {
        angle += offset;
        angle = angle > 360 ? angle - 360 : angle < 0 ? angle + 360 : angle;
        return p.add(GeometryHelper.getVectorFromAngleAndSpeed(angle, speed));
    }
}