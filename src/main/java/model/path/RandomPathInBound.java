package model.path;

import model.geometry.Position;
import model.geometry.Vector;

/**
 * Define a new path changing every time it reach its destination within a rectangle.
 * The new position to reach is randomized.
 */
public class RandomPathInBound extends Path {
    private final float speed;
    private final float x, y, width, height; // box in which a random position is generated
    private Vector currentPath;
    private Position dest;

    public RandomPathInBound(Position startPos, float maxSpeed, float x, float y, float width, float height) {
        this.speed = maxSpeed;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        dest = new Position((float) Math.random() * width + x, (float) Math.random() * height + y);
        currentPath = (new Vector(startPos, dest)).resized(speed);
    }

    @Override
    public Position move(Position p) {
        Vector distance = new Vector(p, dest);
        if (speed * speed >= distance.squareLenght()) {
            dest = new Position((float) Math.random() * width + x, (float) Math.random() * height + y);
            currentPath = (new Vector(p, dest)).resized(speed);
        }
        return p.add(currentPath);
    }
}