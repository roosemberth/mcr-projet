package model.path;

import model.geometry.Position;

/**
 * Define a stationary path. No path basically...
 */
public class StationaryPath extends Path {
    @Override
    public Position move(Position p) {
        return p;
    }
}