package model.options;

/**
 * list of selectable levels.
 */
public enum LevelSelect {
    FirstLevel, SecondLevel
}
