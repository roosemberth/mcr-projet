package model.options;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Contains the information relative to game options.
 * Basically any game effect/modifier defining the initial state of the game is stored here.
 */
@Data
@AllArgsConstructor
public final class Options {
    private final Difficulty difficulty;
    private final ShotType shotType;
    private final float gameSpeed;
    private final LevelSelect levelSelected;

    public Options withGameSpeed(float newGameSpeed) {
        return new Options(difficulty, shotType, newGameSpeed, levelSelected);
    }

    public Options withDifficulty(Difficulty newDifficulty) {
        return new Options(newDifficulty, shotType, gameSpeed, levelSelected);
    }

    public Options withShotType(ShotType newShotType) {
        return new Options(difficulty, newShotType, gameSpeed, levelSelected);
    }

    public Options withLevelSelected(LevelSelect newLevel) {
        return new Options(difficulty, shotType, gameSpeed, newLevel);
    }
}