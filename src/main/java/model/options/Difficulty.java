package model.options;

/**
 * list of difficulty mode.
 */
public enum Difficulty {
    EASY, NORMAL, HARD
}