package model.options;

/**
 * list of player shot types.
 */
public enum ShotType {
    TypeA, TypeB, TypeC
}