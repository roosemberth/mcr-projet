package model.commandMovement;

import model.Input;
import model.gameObjects.GameObject;
import model.geometry.Position;
import model.geometry.Vector;

public abstract class RelativeMovement extends CommandMovement {
    private final GameObject source;
    private Vector relativePosition;

    protected RelativeMovement(GameObject source, Vector initialRelativePosition) {
        this.source = source;
        relativePosition = initialRelativePosition;
    }

    @Override
    public Position move(Position p, Input input) {
        relativePosition = moveRelative(relativePosition, input);
        return source.getPosition().add(relativePosition);
    }

    protected abstract Vector moveRelative(Vector relativePosition, Input input);
}