package model.commandMovement;

import model.Input;
import model.gameObjects.Player;
import model.geometry.Vector;

/**
 * Define the ally movement behavior for the shot type A.
 * It moves in specific location relative to the player.
 * If the player transition between focus and standard movement then the object has to move accordingly.
 */
public class AllyShotTypeAPath extends RelativeMovement {
    private final Vector relativeTarget;
    private final Vector relativeTargetFocus;
    private final float speed;

    public AllyShotTypeAPath(Player owner, Vector relativeTarget, Vector relativeTargetFocus, float speed) {
        super(owner, relativeTarget);
        this.relativeTarget = relativeTarget;
        this.relativeTargetFocus = relativeTargetFocus;
        this.speed = speed;
    }

    @Override
    protected Vector moveRelative(Vector relativePosition, Input input) {
        Vector target = input.isFocus() ? relativeTargetFocus : relativeTarget;
        Vector direction = new Vector(relativePosition, target);
        if (direction.squareLenght() > speed * speed) {
            relativePosition = relativePosition.add(direction.resized(speed));
        } else {
            relativePosition = target;
        }
        return relativePosition;
    }
}