package model.commandMovement;

import lombok.Setter;
import model.Input;
import model.gameObjects.Player;
import model.geometry.Position;

/**
 * Define an object behavior for when there is an input from the user requiring a movement.
 */
public class PlayerMovement extends CommandMovement {
    @Setter
    private Player owner;

    @Override
    public Position move(Position p, Input input) {
        return p.add(input.getX() * owner.getSpeed(), input.getY() * owner.getSpeed());
    }
}