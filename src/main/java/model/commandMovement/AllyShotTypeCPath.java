package model.commandMovement;

import model.Input;
import model.gameObjects.GameObject;
import model.geometry.Position;
import model.geometry.Vector;

/**
 * Define the ally movement behavior for the shot type C.
 * It follow a target (the player or another ally) when it goes too fat away.
 * This create a snake like behavior to the player movement.
 */
public class AllyShotTypeCPath extends CommandMovement {
    private final GameObject target;
    private final float distance;

    public AllyShotTypeCPath(GameObject target, float distance) {
        this.target = target;
        this.distance = distance;
    }

    @Override
    public Position move(Position p, Input inputs) {
        Vector disToTarget = new Vector(target.getPosition(), p);

        if (distance * distance > disToTarget.squareLenght()) {
            return p;
        }

        return target.getPosition().add(disToTarget.resized(distance));
    }
}