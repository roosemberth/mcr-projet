package model.commandMovement;

import model.Input;
import model.geometry.Position;

/**
 * Define an object that require movement based on the input from the user.
 */
public abstract class CommandMovement {
    public abstract Position move(Position p, Input input);
}