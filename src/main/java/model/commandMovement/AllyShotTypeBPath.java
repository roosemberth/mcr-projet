package model.commandMovement;

import model.Input;
import model.gameObjects.Player;
import model.geometry.GeometryHelper;
import model.geometry.Vector;

/**
 * Define the ally movement behavior for the shot type B.
 * It moves around the player in circle counterclockwise.
 * If the player transition between focus it move closer to the player and move clockwise around the player.
 */
public class AllyShotTypeBPath extends RelativeMovement {
    private final float disMax;
    private final float disMin;
    private float angle;
    private float disCurrent;

    public AllyShotTypeBPath(Player owner, float angle, float distance) {
        super(owner, new Vector(0, 0));
        this.angle = angle;
        this.disMax = distance;
        this.disMin = distance / 2;
        this.disCurrent = disMin;
    }

    @Override
    protected Vector moveRelative(Vector relativePosition, Input input) {
        if (input.isFocus() && disCurrent > disMin) {
            disCurrent--;
        } else if (!input.isFocus() && disCurrent < disMax) {
            disCurrent++;
        }
        angle += input.isFocus() ? 1f : -1f;
        angle = angle > 360 ? angle - 360 : angle < 0 ? angle + 360 : angle;
        return GeometryHelper.getVectorFromAngleAndSpeed(angle, disCurrent);
    }
}