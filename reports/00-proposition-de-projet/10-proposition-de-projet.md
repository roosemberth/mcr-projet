---
date: 3 Mar 2021
title: Proposition de projet MCR
subtitle: Groupe pattern Visiteur
author:
- Roosembert Palacios
- Gazzetta Florian
- Wilfried Karel Ngueukam Djeuda
- Junod Christophe
- Mai Hoang Anh
---
## Description du projet

L'idée du projet est de mettre en place un jeu du type [Shoot'em up].

[Shoot'em up]: https://en.wikipedia.org/wiki/Shoot_%27em_up

Dans notre implémentation, nous prévoyons les fonctionnalités
suivantes :

- Un vaisseau contrôlé par le joueur.
- Des ennemis.
- Des niveaux.
- Des points de vies.

Si le temps le permet, nous aurons l’opportunité d’étendre
le jeu avec les fonctionnalités suivantes :

- Statistiques du joueur.
- Classement des joueurs.
- Niveaux supplémentaires.

## Modélisation du système

Voici un schéma conceptuel de ce que nous prévoyons comme
implémentation (à noter qu'il s'agit d'un schéma du système et
non pas d'un schéma de classes) :

![Schéma des composants du système](img/system-diagram.png)

### Utilisation du pattern visiteur

Comme on peut voir dans le schéma présenté ci-dessus, le
`TickController` se chargera faire visiter tous les
`InteractionManager` sur chaque `GameObject`.
